module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            main: {
                files: [
                    'src/scss/**/*.scss',
                    'src/js/**/*.js'
                ],
                tasks: [ 'sass', 'uglify:bundle' ]
            },
            configFiles: {
                files: [ 'Gruntfile.js' ],
                options: {
                    reload: true
                }
            }
        },
        sass: {
            options: {
                sourceMap: false,
                outputStyle: "compressed",
                trace: false,
                debugInfo: true
            },
            all: {
                files: {
                    'public/css/main.min.css': 'src/scss/main.scss'                    
                }
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
                        '<%= grunt.template.today("yyyy-mm-dd") %> */',
                sourceMap: false,
                mangle: {
                    except: ['jQuery']
                },
                compress: {
                    drop_console: false
                }
            },
            bundle: {
                files: [{
                    expand: true,
                    cwd: 'src/js',
                    src: '*.js',
                    dest: 'public/js',
                    ext: '.min.js',
                    extDot: 'last'
                }]
            }
        },
        copy: {
            js: {
                files: [{
                    cwd: 'node_modules/font-awesome/fonts/',
                    src: '*',
                    dest: 'public/fonts/',
                    expand: true
                },{
                    cwd: 'node_modules/font-awesome/css/',
                    src: 'font-awesome.min.css',
                    dest: 'public/css/',
                    expand: true
                }]
            }
        },
        clean: {
            options: {
                'no-write': false
            },
            js: {
                src: ['src/scss/*.css', 'src/scss/*.css.map', 'dist/js/*.min.min.js', 'dist/js/*.min.min.js.map']
            }
        },
    });

    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('default', ['sass:all','uglify:bundle','copy:js','clean:js']);
};
