//importalt komponensek
import React from 'react';
import { DndProvider } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import { BrowserRouter as Router, Route, Link, Switch, Redirect } from "react-router-dom";
import { makeStyles, createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { green, orange } from '@material-ui/core/colors';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Divider from '@material-ui/core/Divider';
import AppBar from '@material-ui/core/AppBar';
import Drawer from '@material-ui/core/Drawer';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Collapse from '@material-ui/core/Collapse';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import PropTypes from 'prop-types';
import ScrollArea from 'react-scrollbar';
import ModalComponent from './modal';

import SnackbarComponent from './snackbar.js';
import PostComponent from './post.js';
import ListComponent from './list.js';
import FilterListItem from './react/list-items/filter';
import FolderListItem from './react/list-items/folder';
import OtherListItem from './react/list-items/other';


/**
 *  
 */
const InfoModal = (props) => {
	let { openedModal, classes, closeModalEditor } = props;
    
    let modalHead = (<h2 id="modal-title">Segítség</h2>);
    let modalBody = (<div id="infoWrapper">
    	<table>
    		<tr>
    			<td>Eredeti poszt megtekintése</td>
    			<td className="kb1">v</td>
    		</tr>
    		<tr>
    			<td>Szerző adatlapjának megtekintése</td>
    			<td className="kb1">a</td>
    		</tr>
    		<tr>
    			<td>Üzenet a szerzőnek</td>
    			<td className="kb1">c</td>
    		</tr>
    		<tr>
    			<td>Lapozás</td>
    			<td className="kb2">zx</td>
    		</tr>
    	</table>
	</div>);

    return(
        <ModalComponent
        openedModal={openedModal}
        modalBody={modalBody}
        modalHead={modalHead}
        closeModalEditor={closeModalEditor}
        classes={classes}
        />
    );
}

/** 
 * Tab komponens (material ui)
 * ----------------------
 * @param {integer} tabIndex:   	aktualis tab numerikus indexe
 * @param {string} tabPath: 		aktualis tab szoveges indexe
 * @param {object} folders: 		betoltott konyvtarak
 * @param {object} filters: 		betoltott filterek
 * @param {object} openedTabs:  	megnyitott tabok
 * @param {object} actions: 		dispatch / subscribe / retain fuggvenyek gyujtoobjektuma
 * @param {object} routing: 		routing cuccok gyujtoobjektuma
 * @param {function} changeTab: 	tabmenu atkattintaskor fut le, befrissiti a state tabIndex es tabPath ertekeit
 * @param {function} tabProps:  	id alapjan visszaad material ui-os tab propertyket
 */
function TabComponent(props) {
	const { tabIndex, tabPath, folders, filters, openedTabs, changeTab, tabProps, allSalestags, ...other } = props;

	return(
 		<div className="contentWrapper">				
	 		<Paper className={useStyles.root}>	        		
		 		<Tabs	                     
		 		variant="scrollable"
		 		value={tabIndex}
		 		onChange={changeTab}
		 		aria-label="Tabs"
		 		indicatorColor="secondary"
		 		className={useStyles.tabs}
		 		>
		 		{
		 			openedTabs.map((item, key) => 
		 				item.id !== null 
		 				? <Tab 
			 				data-path={item.id}
			 				icon={
			 					<i key={key} className={['fa', LoadIcon( isNaN(item.id) ? item.id : 'custom'+item.id ).iconClass].join(" ")} />
			 				}
			 				label={item.params.title}
			 				to={`/${(item.id.indexOf('folder-') !== -1 ? item.id.split('-')[0] + "/" + item.id.split('-')[1] : item.id)}`}
			 				component={Link}
			 				key={key}
			 				{...tabProps(key)} 
			 				/>
		 				: null		 				
	 				)
		 		}
		 		</Tabs>
	 		</Paper>

	 		{
	 			openedTabs.map((item, key) =>                     	
	 				<TabPanel 
	 				key={key}                         
	 				value={tabIndex} 
	 				index={key}                        
	 				>
	 				{
	 					(key === tabIndex 
	 						? <ListComponent
		 						tabIndex={tabIndex} 
		 						tabPath={tabPath} 
		 						allSalestags={allSalestags}
		 						openedTabs={openedTabs} 
		 						folders={folders}
		 						filters={filters}
	 							{...other}
		 						/> 
	 						: null)
	 				}
	 				</TabPanel>
	 				)
	 		}
 		</div>
	);
}


/**
 * tab komponens proptype-jai 
 */
TabComponent.propTypes = {
    tabIndex: PropTypes.any.isRequired,
    tabPath: PropTypes.any.isRequired,
    tabProps: PropTypes.any.isRequired,
    folders: PropTypes.any.isRequired,
    filters: PropTypes.any.isRequired,
    openedTabs: PropTypes.any.isRequired,    
    changeTab: PropTypes.any.isRequired
};


/**
 * tabpanel komponens
 * visszaad egy tabpanelt a megadott parameterek alapjan
 */
 function TabPanel(props) {
 	const { children, value, index, ...other } = props;

 	return (
 		<Typography
 		component="div"
 		role="tabpanel"
 		hidden={value !== index}
 		id={`scrollable-force-tabpanel-${index}`}
 		aria-labelledby={`scrollable-force-tab-${index}`}
 		{...other}
 		>
 		<Box p={3}>{children}</Box>
 		</Typography>
 		);
 }


/**
 * tabpanel proptype-jai 
 */
 TabPanel.propTypes = {
 	children: PropTypes.node,
 	index: PropTypes.any.isRequired,
 	value: PropTypes.any.isRequired,
 };



/** 	 
 * visszadja egy adott menupont font-awesome-os ikonjat
 * ----------------------    	 
 * @param {string} path: szoveges tab index
 * @return {object}: ha a szoveges tab index alapjan filterre keresunk, megkeressuk az egyezo elemet az iconObj-ben,
 * 					 ha a szoveges tab index alapjan konyvtarra keresunk, visszaadunk egy statikus objektumot
 */
 function LoadIcon(path = "all") {
    if (path.indexOf('folder-') === -1) { //ha filter
    	if(path.indexOf('custom') === -1) { //ha default filter
    		const iconsObj = [{"id": 0, "path": "all", "iconClass": "fa-globe" },  
    		{"id": 1, "path": "hirdeteses", "iconClass": "fa-usd" },  
    		{"id": 2, "path": "holdudvar", "iconClass": "fa-users" },  
    		{"id": 3, "path": "kitiltottak", "iconClass": "fa-ban" },  
    		{"id": 4, "path": "copycat", "iconClass": "fa-files-o" },  
    		{"id": 5, "path": "nosalestagall", "iconClass": "fa-window-close" },  
    		{"id": 6, "path": "nosalestagfiltered", "iconClass": "fa-window-close" }];

    		return iconsObj.find((element) => {
    			return element.path === path;
    		})	    		
    	} else { //ha custom filter
    		return { "id": 7, "path": "filter", "iconClass": "fa-filter" };	    		
    	}
    } 	    
    else { //ha konyvtar
    	return { "id": 8, "path": "folder", "iconClass": "fa-folder" };
    }		
}



//drawer szelessege
const drawerWidth = 240;

//fo szinek
const orangeTheme = createMuiTheme({
	palette: {
		primary: {
			main: '#f47629'
		},
		secondary: {
			main: '#f47629'
		}
	}
})

//material ui styles
const useStyles = makeStyles(theme => ({
	root: {
		flexGrow: 1,
		color: orange[500],
		backgroundColor: theme.palette.background.paper,
		display: 'flex',
		height: 224
	},
	appBar: {
		transition: theme.transitions.create(['margin', 'width'], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen,
		}),
	},
	appBarShift: {
		width: `calc(100% - ${drawerWidth}px)`,
		marginLeft: drawerWidth,
		transition: theme.transitions.create(['margin', 'width'], {
			easing: theme.transitions.easing.easeOut,
			duration: theme.transitions.duration.enteringScreen,
		}),
	},
	menuButton: {
		marginRight: theme.spacing(2),
	},
	hide: {
		display: 'none',
	},
	drawer: {
		width: drawerWidth,
		flexShrink: 0,
	},
	drawerPaper: {
		width: drawerWidth,
	},
	drawerHeader: {
		display: 'flex',
		alignItems: 'center',
		padding: '0 8px',
		...theme.mixins.toolbar,
		justifyContent: 'flex-end',
	},
	content: {
		flexGrow: 1,
		padding: theme.spacing(3),
		transition: theme.transitions.create('margin', {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen,
		}),
		marginLeft: -drawerWidth,
	},
	contentShift: {
		transition: theme.transitions.create('margin', {
			easing: theme.transitions.easing.easeOut,
			duration: theme.transitions.duration.enteringScreen,
		}),
		marginLeft: 0,
	},
	nested: {
		paddingLeft: theme.spacing(4),
	},
	tabs: {
		borderRight: `1px solid ${theme.palette.divider}`,
	},
	icon: {
		fontSize: 20,
		margin: theme.spacing(2),
	},
	badge: {
		padding: theme.spacing(0, 0)
	},
	formControl: {
		margin: theme.spacing(1),
		minWidth: '100%',
		maxWidth: 300,
	},
	chips: {
		display: 'flex',
		flexWrap: 'wrap',
	},
	chip: {
		margin: 2,
	},
	success: {
		backgroundColor: green[600],
	},
	error: {
		backgroundColor: theme.palette.error.dark,
	},
	iconVariant: {
		opacity: 0.9,
		marginRight: theme.spacing(1),
	},
	message: {
		display: 'flex',
		alignItems: 'center',
	},
	modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
	buttonRipple: {
		color: '#f47629'
	}
}));


export default class GlobalComponent extends React.Component {
   /*
	* event listener - subscriber
	* ha ugyanaz az event neve, akkor hozzacsatolja az objectbe
	*/
	actions = (function(scope) {
		let eventStore = [],
		appContainer = document.getElementById('root') || window;

		return {
			dispatch : function(type, data){
				let dispatchEvent = null,
				hasEvent = eventStore.find((event) => {
					return event.type === type;
				});

				if (hasEvent !== undefined){
					dispatchEvent = hasEvent.event;
					dispatchEvent.payload = data;
				} else{
					dispatchEvent = new CustomEvent(type, { detail : 'BlogReaderCustomEvent:' + type });
					dispatchEvent.payload = data;
					eventStore.push({
						type : type,
						event : dispatchEvent
					});
				}

				// console.log('dispatching events', eventStore);
				appContainer.dispatchEvent(dispatchEvent);
			},

			subscribe : function(type, callBack){
				appContainer.addEventListener(type, (event) => {
					if (typeof callBack === 'function'){
						callBack(event.payload);
					}
				});
			},

			retain : function(type, callBack){
				let event = eventStore.find((event) => {
					return event.type === type;
				});
				if (event !== undefined){
					let eventIndex = eventStore.findIndex((event) => {
						return event.type === type;
					});

					appContainer.removeEventListener(type, (typeof callBack === 'function' ? callBack : null));
					if (eventIndex !== -1){
						eventStore.splice(eventIndex, 1);
					}
				}
			}
		}
	}(this))	



	/**
	 * a constructor state-jeben tarolunk minden erteket, 
	 * amelyre a komponensunknek szuksege lesz
	 */
	 constructor(props) {
	 	super(props);

	 	this.state = {
			version: "dev", //verzio tipusa (dev / prod)
			drawerOpen: true, //sidebar (nyitva / zarva)
			
			infoModal: true, //info modal (nyitva / zarva)

			customFilterId: null, //sajat szuro azonosito
			customFilterMenu: false, //sajat szuro jobb-klikk menu
			customFilterMenuAnchor: null, //sajat szuro jobb-klikk menu pozicio
			collapseFiltersOpen: false, //osszecsukhato menu nyitva / zarva
			
			folderId: null, //konyvtar azonosito
			folderMenu: false, //konyvtar jobb-klikk menu
			folderMenuAnchor: null, //konyvtar jobb-klikk menu pozicio
			
			//megnyitott tabok
			openedTabs: [{
				"id": "all",
				"params": {
					"title": "Összes",
					"tabIndex": 0
				}
			}],
			tabIndex: 0, //aktualis tab indexe
			tabPath: 'all', //aktualis tab neve

			page: 1, //aktualis tab oldalszama
			
			loadedItems: [], //betoltott elemek
			allSalestags: [], //betoltott salestagek
			filters: [], //betoltott szurok
			folders: [], //betoltott mappak

			// viewType: 'list', //nezet tipus

			//snackbar
			dialog: {
				message: '', //uzenet
				variant: '' //tipus (error / success)
			},

			//komponens render
			render: {
				list: true,
				tabs: true,
				post: true
			},

			loading: true //betoltes ikon
		};
		
		//bindoljuk a fuggvenyeket, amelyek maguktol nem latnak a this-t
		this.loadApi = this.loadApi.bind(this);	
		this.handleDrawerOpen = this.handleDrawerOpen.bind(this);
		this.handleDrawerClose = this.handleDrawerClose.bind(this);
		this.openedTabsController = this.openedTabsController.bind(this);
		this.removeFilter = this.removeFilter.bind(this);
		this.removeFolder = this.removeFolder.bind(this);
		this.handleUnmount = this.handleUnmount.bind(this);		
	}


	/**
	 * itt minden lefut, amint a komponens mountolodott
	 */
	 componentDidMount() {

        //lehivjuk az osszes salestaget es letaroljuk a state-ben
        this.loadSidebarApi('salestag');
		
		//betoltjuk a filtereket es letaroljuk a state-ben
        this.loadSidebarApi('filter');
        
        //betoltjuk a mappakat es letaroljuk a state-ben / local storage-ban
        this.loadSidebarApi('folder');

        //befrissitjuk az oldalsav szuroit
        this.actions.subscribe('refreshFilters', (data) => {
        	this.loadSidebarApi('filter');
        });

        //befrissitjuk az oldalsav mappait
        this.actions.subscribe('refreshFolders', (data) => {
        	this.loadSidebarApi('folder');
        });

        //osszes poszt frissitese
        this.actions.subscribe('loadedItems', (data) => {
        	this.setState({ 
        		loadedItems: data
        	});
        });

        //megnyitjuk a snackbart
        this.actions.subscribe('openDialog', (data) => {
        	this.handleDialog('variant', data.variant);
        	this.handleDialog('message', data.message);
        });

        //bezarjuk a snackbart
        this.actions.subscribe('closeDialog', (data) => {
        	this.handleDialog('variant', '');
        	this.handleDialog('message', '');
        });
    }


    /**
     * itt minden lefut, amint a komponens unmountolodik
     */
    componentWillUnmount() {
    	this.actions.retain('refreshFilters');
    	this.actions.retain('openedModalFilterEditor');
    	this.actions.retain('openedTabs');
    	this.actions.retain('tabIndex');
    }


    /**
     * dialog kezelo
     * atallitja a state.dialog object valamely tagjanak az erteket
     * ----------------------        
     * @param {string} key:         state.dialog object tagjanak a neve (message / variant)
     * @param {string} value:       state.dialog object tagjanak az erteke
     */
    handleDialog(key, value) {
    	this.setState(prevState => ({
    		dialog: {
    			...prevState.dialog,
    			[key]: value
    		}
    	}))
    }


    /**
     * unmount kezelo
     * atallitja a state.render object valamely tagjanak az erteket false-ra
     * ----------------------        
     * @param {string} key:         state.render object tagjanak a neve (list / tabs / post)
     */
    handleUnmount(key) {
    	this.setState(prevState => ({
    		render: {
    			...prevState.render,
    			[key]: false
    		}
    	}))
    }


    /**
     * reader action hivas
     * ----------------------        
     * @param {string} url:         action url-je
     * @param {string} method:      action tipusa (PUT / DELETE)
     * @param {object} bodyParams:  body param-ok atadasahoz (opcionalis)
     * @param {function} callback:  callback fv.
     */
    action(url, method, body = null, callback) {
     	var requestParams = new Request("/reader/" + url, {
            method: method,
            headers: { 
                "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
            },
            body: body
        });

        var request = fetch(requestParams);

        request.finally(() => {
            setTimeout(function() {
                callback();
            }, 1000);
        });
    }


    /**
     * api betoltese
     * ----------------------        
     * @param {string} url:      	json url-je
     * @param {function} callback:  callback
     */
    loadApi = (url, callback) => {
 		fetch(url)
 		.then(res => res.json())
 		.then(
 			(result) => {
 				callback(result);
 			}
		)
    }


    /**
     * oldalsav feedjeinek betoltese tipus alapjan
     * ----------------------        
     * @param {string} type:      	tipus neve (filter / folder / salestag)
     */
    loadSidebarApi(type) {
    	let apiUrl, action;
    	let { version } = this.state;
    	let filterUrl = (version === "dev" ? "./json/filter.json" : "/reader/filter");
	 	let folderUrl = (version === "dev" ? "./json/folder.json" : "/reader/folder");		
	 	let salestagsUrl = (version === "dev" ? "./json/salestags.json" : "/reader/post-list/list-salestags"); 

	 	switch (type) {
	 		default:
	 		case 'filter':
	 		apiUrl = filterUrl;
	 		break;

	 		case 'folder':
	 		apiUrl = folderUrl;
	 		break;

	 		case 'salestag':
	 		apiUrl = salestagsUrl;
	 		break;
	 	}

    	this.loadApi(apiUrl, (result) => {
		 	switch (type) {
	 			default:
		 		case 'filter':
		 		action = { filters: result };
 				this.actions.dispatch('refreshedFilters', result);
		 		break;

		 		case 'folder':
		 		action = { folders: result };
 				this.actions.dispatch('refreshedFolders', result);
		 		break;

		 		case 'salestag':
		 		action = { allSalestags: result };
		 		break;
		 	}

 			this.setState(action);
 		});
    }


	/**
     * torol egy custom filtert
     * ----------------------        
     * @param {integer} filterID:     filter numerikus azonositoja     
     */
    removeFilter(filterID) {
     	var self = this;     	

     	this.setState({ //kiuritjuk a filter state-et
     		filters: []
     	});

     	var deleteRequest = new Request("/reader/filter/delete/?id=" + filterID, {
     		method: "DELETE"
     	});
     	
     	var request = fetch(deleteRequest);

     	//ha lefutott befrissitjuk a filters state-et
     	request.finally(() => {
     		setTimeout(self.loadSidebarApi('filter'), 500);
     		this.actions.dispatch('openDialog', {
     			message: `Szűrő törölve`,
     			variant: 'success',
     			opened: true
     		});
     	});
	}


	/**
     * torol egy mappat
     * ----------------------        
     * @param {integer} folderID:     folder numerikus azonositoja     
     */
    removeFolder(folderID) {
     	var self = this;     	

     	this.setState({ //kiuritjuk a filter state-et
     		folders: []
     	});

     	var deleteRequest = new Request("/reader/folder/delete/?id=" + folderID, {
     		method: "DELETE"
     	});
     	
     	var request = fetch(deleteRequest);

     	//ha lefutott befrissitjuk a filters state-et
     	request.finally(() => {
     		setTimeout(self.loadSidebarApi('folder'), 500);
     		this.actions.dispatch('openDialog', {
     			message: `Mappa törölve`,
     			variant: 'success',
     			opened: true
     		});
     	});
	}


    /**
     * oldalso menu kinyitasa
	 * igazra allitja a drawerOpen erteket a stateben
	 */
	handleDrawerOpen() {
	 	this.setState({
	 		drawerOpen: true
	 	});
	 }


	/**
	 * oldalso menu bezarasa
	 * hamisra allitja a drawerOpen erteket a stateben
	 */
	handleDrawerClose() {
	 	this.setState({
	 		drawerOpen: false
	 	});
	 }


    /** 	 
 	 * sajat szurok lenyilo menujenek toggle-zese
 	 */
 	toggleCollapseFilters = () => {
 	 	this.setState({
 	 		collapseFiltersOpen: !this.state.collapseFiltersOpen
 	 	});
 	}


    /**
 	 * megnyitott tabok kattintas esemenye
 	 * befrissitjuk a state-ben tarolt tabIndex es tabPath ertekeket
 	 * ----------------------    	 
	 * @param {object} event:  kattintas esemeny objektuma
	 * @param {integer} value: numerikus tab index
	 */
	changeTab = (event, value) => {
	 	let target = event.target.closest('a[data-path]');
	 	let tabPath = target.getAttribute('data-path');

	 	if (tabPath !== false) {
	 		this.setState({
	 			tabIndex: value,
	 			tabPath: tabPath
	 		}, () => {
	            // console.log('tabIndex: ', this.state.tabIndex)
	            // console.log('tabPath: ', this.state.tabPath)
	        });        	
	 	}
	 }


    /**
 	 * tabok bezarasanak kattintas esemenye
 	 * befrissitjuk a state-ben tarolt openedTabs, tabIndex es tabPath ertekeket
 	 * ----------------------    	 
	 * @param {string} path:   szoveges tab index	 
	 *
    closeTab(path) {
        const items = this.state.openedTabs.filter(item => item.id !== path);
        
        this.setState({ 
            openedTabs: items, 
            tabPath: 'all',
            tabIndex: 0
        }, () => {
            // this.changeTab(null, 0);
        });        
    }*/

    
    /**
 	 * elem keresese tombben, id alapjan
 	 * ----------------------    	 
	 * @param {array} array: a tomb, amiben keresunk
	 * @param {string} prop: az objektum propja
	 * @param {string} id: 	 az id, amelyre keresunk (valojaban id === path)
	 * @return {object}: 	 az object a tombben, amelynek megegyezik az id-ja
	 */
	findArrayElementById(array, prop, id) {
	 	return array.find((element) => {
	 		return element[prop] === id;
	 	})
	 }


    /**
 	 * az oldalso menupontok kattintas fuggvenye,
 	 * amely tabkent nyit meg minden megkattintott menupontot,
 	 * illetve befrissiti a stateben rogzitett tabIndex-et es tabPath-t
 	 * ----------------------    	 
	 * @param {object} event: a klikk esemeny objektuma
	 */
	openedTabsController(event) {    	
	 	let target 	 = event.target.parentElement.closest('a');
	 	let title	 = target.getAttribute('label');
	 	let path	 = target.getAttribute('data-path');
	 	let isOpened = this.findArrayElementById(this.state.openedTabs, 'id', path);

    	//aktiv post id resetelese
    	if (localStorage.getItem('post_ID') !== null && localStorage.getItem('post_ID') !== undefined) {    		
    		localStorage.setItem('post_ID', '');
    	}

    	//ha a tab meg nincs megnyitva, megnyitjuk
    	if (isOpened === undefined) {
    		let recentOpenedTab = [{
    			"id": path,
    			"params": {
    				"title": title,
    				"tabIndex": this.state.openedTabs.length
    			}
    		}];

    		this.setState(prevState => ({
    			tabIndex: this.state.openedTabs.length,
    			tabPath: path,
    			openedTabs: [...prevState.openedTabs].concat(recentOpenedTab)
    		}), () => {
    			// console.log('openedTabs: ', this.state.openedTabs);
    			this.actions.dispatch('tabIndex', this.state.tabIndex);
    			this.actions.dispatch('tabPath', this.state.tabPath);
    			this.actions.dispatch('openedTabs', this.state.openedTabs);
    		});
    	} else { //ha a tab meg van megnyitva, tabot valtunk
    		let tabIndex = this.findArrayElementById(this.state.openedTabs, 'id', path).params.tabIndex;    		

    		//osszehasonlitjuk a tabIndexet es tabot cserelunk, ha elter
    		if (!isNaN(tabIndex) === true && this.state.tabIndex !== tabIndex) {
    			this.changeTab(event, tabIndex);
    		}
    	}	 		
    }


    /** 	 
 	 * id alapjan visszaad material ui-os tab propertyket
 	 * ----------------------    	 
	 * @param {integer} index: numerikus tab index
	 * @return {object}: a propertyk, amelyeket a material ui-os tab komponensunk feldolgoz
	 */
	tabProps(index) {
	 	return {
	 		id: `scrollable-force-tab-${index}`,
	 		'aria-controls': `scrollable-force-tabpanel-${index}`,
	 	};
	 }


    /** 	 
 	 * bejelentjuk, hogy az addCustomFilter erteke igaz,
 	 * igy erre az esemenyre fel tudunk iratkozni a lista komponens componentDidMount-jaban
 	 */
 	openModalFilterEditor = () => {
 	 	this.actions.dispatch('addCustomFilter', true);
 	}


 	/** 	 
 	 * bejelentjuk, hogy az addFolder erteke igaz,
 	 * igy erre az esemenyre fel tudunk iratkozni a lista komponens componentDidMount-jaban
 	 */
 	openModalFolderEditor = () => {
 	 	this.actions.dispatch('addFolder', true);
 	}


    /** 	 
     * megnyit egy felugro menut a sajat szuroknel (torleshez / szerkeszteshez) a jobb egerkattintasra
 	 * ----------------------    	 
	 * @param {object} event: 	click event
	 */
	openCustomFilterMenu = (event) => {
	 	let customFilterId = parseInt(event.target.closest('a').getAttribute('data-path').split('custom')[1]);

	 	event.preventDefault(); 

	 	this.setState({
	 		customFilterId: customFilterId,
	 		customFilterMenu: true,
	 		customFilterMenuAnchor: event.currentTarget
	 	});
	}


    /** 	 
     * megnyit egy felugro menut a mappaknal (torleshez / szerkeszteshez) a jobb egerkattintasra
 	 * ----------------------    	 
	 * @param {object} event: 	click event
	 */
	openFolderMenu = (event) => {
	 	let folderId = parseInt(event.target.closest('a').getAttribute('data-path').split('folder-')[1]);

	 	event.preventDefault(); 

	 	this.setState({
	 		folderId: folderId,
	 		folderMenu: true,
	 		folderMenuAnchor: event.currentTarget
	 	});
	}


    /** 	 
 	 * sajat szurok felugro menujenek bezarasa
 	 */
 	closeCustomFilterMenu = () => {
 	 	this.setState({
 	 		customFilterMenu: false,
 	 		customFilterId: null
 	 	});
 	}


 	/** 	 
 	 * mappak felugro menujenek bezarasa
 	 */
 	closeFolderMenu = () => {
 	 	this.setState({
 	 		folderMenu: false,
 	 		folderId: null
 	 	});
 	}


 	/** 	 
 	 * snackbar bezarasa (3mp)
 	 */
    closeSnackbar() {
    	let self = this;

    	setTimeout(() => {
    		self.actions.dispatch('closeDialog', true);
    	}, 3000);
    }


    /** 	 
     * megnyit egy felugro menut a sajat szuroknel (torleshez / szerkeszteshez) a jobb egerkattintasra
 	 * ----------------------    	 
	 * @param {object} event: 	click event
	 */
 	editCustomFilter = (event) => {
 		this.actions.dispatch('editCustomFilter', true); 		

 		this.setState({
 	 		customFilterMenu: false
 	 	});
 	}


 	/** 	 
     * megnyit egy felugro menut a mappaknal (torleshez / szerkeszteshez) a jobb egerkattintasra
 	 * ----------------------    	 
	 * @param {object} event: 	click event
	 */
 	editFolder = (event) => {
 		this.actions.dispatch('editFolder', true);

 		this.setState({
 	 		folderMenu: false
 	 	});
 	}


    /** 	 
 	 * sajat szuro torlesekor feldob egy confirm ablakot
 	 * ----------------------    	 
	 * @param {object} event: 	click event
 	 */
 	deleteCustomFilter = (event) => {		
 	 	var r = window.confirm("Biztosan törlöd ezt a szűrőt?");

 	 	if (r === true) {			
 	 		this.removeFilter(this.state.customFilterId);
 	 	}

		this.closeCustomFilterMenu(); //menu bezarasa		
	}


	/** 	 
 	 * mappa torlesekor feldob egy confirm ablakot
 	 * ----------------------    	 
	 * @param {object} event: 	click event
 	 */
 	deleteFolder = (event) => {
 		let { openedTabs, folderId } = this.state;
 	 	var r = window.confirm("Biztosan törlöd ezt a mappát?");

		if (r === true) {			
 	 		this.removeFolder(this.state.folderId); //mappa törlése

 	 		//kiszedjuk, ha a tabok közt meg van nyitva
 	 		if (this.findArrayElementById(openedTabs, 'id', 'folder-' + folderId)) { 
 				let remainingTabs = openedTabs.filter(item => item.id !== 'folder-' + folderId);

 				this.setState({
 					openedTabs: remainingTabs,
					tabPath: 'all',
 					tabIndex: 0
 				}, () => {
 					this.loadSidebarApi('folder');
 				});
 	 		}
 	 	}

		this.closeFolderMenu(); //menu bezarasa
	}


    /**
     * hozzaad egy posztot egy folderhez
     * ----------------------        
     * @param {integer} postID:     poszt numerikus azonositoja
     * @param {integer} folderID:   mappa numerikus azonositoja
     * @param {integer} blogID:     blog numerikus azonositoja
     */
    addFolder(postID, folderID, blogID, callback) {
        this.action("folder/add-item?folder_id=" + folderID, "PUT", "post_id=" + postID + "&blog_id=" + blogID, function() {
            callback();
        });    
    }


    /**
     * athelyezi a posztot egy masik mappaba
     * ----------------------        
     * @param {integer} currentFolderID:    jelenlegi mappa numerikus azonositoja
     * @param {integer} newFolderID:        uj mappa numerikus azonositoja
     * @param {integer} postID:             poszt numerikus azonositoja
     * @param {integer} blogID:             blog numerikus azonositoja
     */
    changeFolder(currentFolderID, newFolderID, postID, blogID, callback) {
        this.action("folder/move-item?post_id=" + postID + "&current_folder_id=" + currentFolderID + "&new_folder_id=" + newFolderID + "&blog_id=" + blogID, "PUT", null, function() {
            callback();
        });    
    }


    /** 	 
 	 * tab komponens behivasa
 	 * atadjuk az osszes relevans stateben rogzitett erteket
 	 * ----------------------    	 
	 * @param {object} props: routingos cuccok, amelyeket továbbadunk a tab komponensnek
	 */
	loadFeeds = (props) => {		
	 	let { filters, folders, tabPath, tabIndex, openedTabs, tabProps, changeTab, allSalestags, ...other } = this.state;
	 	
	 	if (!tabPath) {
	 		tabPath = props.location.pathname;
	 	}

	 	let listComp = <TabComponent	 		
				 		tabIndex={tabIndex}
				 		tabPath={tabPath}
				 		openedTabs={openedTabs} 
				 		folders={folders}
				 		filters={filters}
				 		routing={props}
				 		allSalestags={allSalestags}
				 		tabProps={this.tabProps}
				 		action={this.action}
				 		actions={this.actions}
				 		changeTab={this.changeTab}
				 		handleUnmount={this.handleUnmount}
				 		addFolder={this.addFolder}
				 		changeFolder={this.changeFolder}
						{...other}
				 		/>

	 	return this.state.render.list ? listComp : null;
	}


    /** 	 
 	 * post komponens behivasa 	 
 	 * ----------------------    	 
	 * @param {object} props: routingos cuccok, amelyeket továbbadunk a tab komponensnek
	 */
	loadItems = (props) => {
		let { loadedItems, allSalestags, folders, version, ...other} = this.state;
		let postComp = <PostComponent
						loadedItems={loadedItems}
				 		allSalestags={allSalestags}
				 		version={version}
				 		useStyles={useStyles} 
				 		action={this.action}
				 		actions={this.actions}
				 		folders={folders}
				 		routing={props}
				 		handleUnmount={this.handleUnmount}
				 		addFolder={this.addFolder}
				 		changeFolder={this.changeFolder}
				 		{...other}
				 		/>;

	 	return this.state.render.post ? postComp : null;
	}


    /** 	 
 	 * itt rendereljuk ki a react router wrappert egy material ui-os drawer komponensbe, benne az utvonalakkal, menupontokkal
 	 */
 	render() {
 		let { drawerOpen, filters, collapseFiltersOpen, customFilterMenu, folderMenu, customFilterMenuAnchor, folderMenuAnchor } = this.state;

 		let modal = (<InfoModal
 					classes={useStyles}
 					closeModalEditor={ () => { this.setState({ infoModal: false }); } }
		 			openedModal={this.state.infoModal}
		 			/>);

 	 	return (
 	 		<ScrollArea
            speed={0.8}            
            horizontal={false}
            >
            <DndProvider backend={HTML5Backend}>
 	 		<MuiThemeProvider theme={orangeTheme}>
	 	 		<div className={useStyles.root}>
		 	 		<Router>
			 	 		<CssBaseline />

			 	 		<AppBar
			 	 		position="fixed"
			 	 		className={(drawerOpen === true ? 'opened' : '')}
			 	 		>
				 	 		<Toolbar>
					 	 		<IconButton
					 	 		edge="start"
					 	 		color="inherit"
					 	 		aria-label="open drawer"
					 	 		onClick={this.handleDrawerOpen}
					 	 		className={(drawerOpen === true ? 'hidden' : '')}
					 	 		>
					 	 			<MenuIcon />
					 	 		</IconButton>

					 	 		<Typography variant="h6" className="logoWrapper" noWrap>
					 	 			<a href="/" id="logo-link">
					 	 				<span id="logo" /> <span>reader</span>
					 	 			</a>
					 	 		</Typography>
				 	 		</Toolbar>
			 	 		</AppBar>

			 	 		<Drawer
			 	 		className={[useStyles.drawer, 'drawer'].join(' ')}
			 	 		variant="persistent"
			 	 		anchor="left"
			 	 		open={drawerOpen}
			 	 		classes={{
			 	 			paper: useStyles.drawerPaper,
			 	 		}}
			 	 		>
			 	 			<ScrollArea
				            speed={0.8}
				            horizontal={false}
				            >				            
					 	 		<div className={useStyles.drawerHeader}>
						 	 		<IconButton onClick={this.handleDrawerClose}>
						 	 			<ChevronLeftIcon />
						 	 		</IconButton>
					 	 		</div>

				 	 			<Divider />

					 	 		<List
					 	 		component="nav"
					 	 		aria-labelledby="nested-list-subheader"
					 	 		subheader={
					 	 			<ListSubheader component="div">
					 	 			Szűrők
					 	 			</ListSubheader>
					 	 		}
					 	 		>		                    	
						 	 		<OtherListItem
						 	 		iconName="fa-plus"
						 	 		title="Új szűrő létrehozása"
						 	 		onClickFunction={this.openModalFilterEditor}
						 	 		/>

						 	 		{filters.map((item, index) => (
						 	 			isNaN(item.id)
						 	 			? <Link
						 	 			  key={index}
								          data-id={index}								          
								          data-path={(isNaN(item.id) ? item.id : 'custom' + item.id)}
								          label={item.params.title} 
								          onClick={this.openedTabsController} 
								          to={`/${(typeof item.id !== "number" ? item.id : "custom" + item.id)}`}
								          >
						 	 				<FilterListItem
						 	 				key={index}
						 	 				index={index}
						 	 				item={item}
						 	 				LoadIcon={LoadIcon}
						 	 				openedTabsController={this.openedTabsController}
						 	 			    />
						 	 			</Link>
						 	 			: null
					 	 			))}

						 	 		<OtherListItem						 	 		
						 	 		iconName="fa-filter"
						 	 		title="Saját szűrők"
						 	 		onClickFunction={this.toggleCollapseFilters}
						 	 		collapseFiltersOpen={collapseFiltersOpen}
						 	 		/>

						 	 		<Collapse in={collapseFiltersOpen} timeout="auto" unmountOnExit>
							 	 		<List component="div" disablePadding>
								 	 		<Menu
								 	 		id="customFilterMenu"
								 	 		anchorEl={customFilterMenuAnchor}
								 	 		keepMounted
								 	 		open={Boolean(customFilterMenu)}
								 	 		onClose={this.closeCustomFilterMenu}
								 	 		>
									 	 		<MenuItem onClick={this.editCustomFilter}>Szerkesztés</MenuItem>
									 	 		<MenuItem onClick={this.deleteCustomFilter}>Törlés</MenuItem>
								 	 		</Menu>

								 	 		{this.state.filters.map((item, index) => (
								 	 			!isNaN(item.id)
								 	 			? <Link
								 	 			  key={index}
										          data-id={index}								          
										          data-path={(isNaN(item.id) ? item.id : 'custom' + item.id)}
										          label={JSON.parse(item.params).title} 
										          onClick={this.openedTabsController}
										          onContextMenu={this.openCustomFilterMenu} 
										          to={`/${(typeof item.id !== "number" ? item.id : "custom" + item.id)}`}
										          >
								 	 				<FilterListItem
								 	 			    key={index}
								 	 				index={index}
								 	 				item={item}
								 	 				LoadIcon={LoadIcon}
								 	 				openedTabsController={this.openedTabsController}
								 	 			  />
								 	 			</Link>
								 	 			: null
							 	 			))}
							 	 		</List>
						 	 		</Collapse>
				 	 			</List>

				 	 			<Divider />

					 	 		<List
					 	 		id="folder-list"
					 	 		component="nav"
					 	 		aria-labelledby="nested-list-subheader"
					 	 		subheader={
					 	 			<ListSubheader component="div">
					 	 			Mappák
					 	 			</ListSubheader>
					 	 		}
					 	 		>
						 	 		<OtherListItem
						 	 		iconName="fa-plus"
						 	 		title="Új mappa létrehozása"
						 	 		onClickFunction={this.openModalFolderEditor}
						 	 		/>

					 	 			<Menu
						 	 		id="folderMenu"
						 	 		anchorEl={folderMenuAnchor}
						 	 		keepMounted
						 	 		open={Boolean(folderMenu)}
						 	 		onClose={this.closeFolderMenu}
						 	 		>
							 	 		<MenuItem onClick={this.editFolder}>Szerkesztés</MenuItem>
							 	 		<MenuItem onClick={this.deleteFolder}>Törlés</MenuItem>
						 	 		</Menu>						 	 	

						 	 		{this.state.folders.map((item, index) => (
						 	 			<Link						 	 			
						 	 			key={index}
						 	 			data-id={index}
						 	 			data-path={`folder-${item.id}`} 
						 	 			data-type="folder"
						 	 			label={item.name} 
						 	 			onClick={this.openedTabsController} 
						 	 			onContextMenu={this.openFolderMenu}
						 	 			to={`/folder/${item.id}`}
						 	 			>
							 	 			<FolderListItem
							 	 			key={index}
							 	 			id={item.id}
							 	 			name={item.name}
							 	 			badgeContent={item.post_count}
							 	 			actions={this.actions}
							 	 			openedTabsController={this.openedTabsController}
							 	 			openFolderMenu={this.openFolderMenu}
							 	 			/>						 	 			
						 	 			</Link>
					 	 			))}
					 	 		</List>
					 	 	</ScrollArea>	
			 	 		</Drawer>

			 	 		<main className={drawerOpen === true ? 'opened' : ''}>			 	 		
				 	 		<div className={useStyles.drawerHeader} />
					 	 		<Switch>	                       
						 	 		<Route exact path="/" render={() => (<Redirect to="/all" />)} />
						 	 		<Route exact path="/reader/web/build/" render={() => (<Redirect to="/all" />)} />

						 	 		{filters.map((item, index) => (
						 	 			typeof item.id !== "number" 
						 	 			? <Route key={index} exact path={`/${item.id}`} component={this.loadFeeds} />
						 	 			: <Route key={index} exact path={`/custom${item.id}`} component={this.loadFeeds} />
					 	 			))}

						 	 		{filters.map((item, index) => (
						 	 			<Route key={index} exact path={`/${item.id}/:id`} component={this.loadItems} />
					 	 			))}

						 	 		<Route key={filters.length} exact path={`/folder/:id`} component={this.loadFeeds} />
					 	 		</Switch>

					 	 		{
					 	 			this.state.dialog.variant !== '' && this.state.dialog.message !== ''
					 	 			? <SnackbarComponent
							 	 	   classes={useStyles}
							 	 	   className={useStyles.margin}
							 	 	   message={this.state.dialog.message}
							 	 	   variant={this.state.dialog.variant}
							 	 	   actions={this.actions}
							 	 	   closeSnackbar={this.closeSnackbar}
							 	 	   />
					 	 			: null
					 	 		}

					 	 		{this.state.infoModal ? modal : null}					 	 		
			 	 		</main>
		 	 		</Router>
	 	 		</div>
 	 		</MuiThemeProvider>
 	 		</DndProvider>
 	 		</ScrollArea>
	 		);
	}
}