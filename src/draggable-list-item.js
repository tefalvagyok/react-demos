import React from 'react'
import { useDrag } from 'react-dnd'
import ListItem from '@material-ui/core/ListItem';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

export const ItemTypes = {
    BOX: 'box'
};

const DraggableListItem = (props) => {
    const [{ isDragging }, drag] = useDrag({
        item: { 
            post_id: props.item.post_ID, 
            post_title: props.item.post_title, 
            folders: props.item.folders, 
            type: ItemTypes.BOX 
        },
        end: (item, monitor) => {
            const dropResult = monitor.getDropResult();

            if (item && dropResult) {
                let post = props.findArrayElementById(props.loadedItems, 'post_ID', item.post_id); //megkeressuk a posztot
                
                if (post !== undefined) { 
                    let result = props.findArrayElementById(post.folders, 'id', dropResult.folder_id); //megnezzuk hogy a poszt rendelkezik-e a valasztott folder id-val
                        
                    if (result === undefined) { //nem rendelkezik, hozzaadhatjuk a konyvtarhoz
                        if (props.tabPath.indexOf('folder-') !== -1) { //ha konyvtar tabon allunk, athelyezes
                            props.changeFolder(props.tabPath.replace('folder-',''), dropResult.folder_id, props.item.post_ID, props.item.post_blog_ID, function() {
                                props.actions.dispatch('refreshFolders', true);
                                props.actions.dispatch('refreshTabs', true);
                                props.actions.dispatch('openDialog', {
                                    message: `A(z) ${item.post_title} című poszt átkerült a(z) ${dropResult.name} könyvtárba!`,
                                    variant: 'success',
                                    opened: true
                                });

                            });                        
                        } else { //ha szuro tabon allunk, hozzaadas
                            props.action("folder/add-item?folder_id=" + dropResult.folder_id, "PUT", "post_id=" + props.item.post_ID + "&blog_id=" + props.item.post_blog_ID, function() {
                                props.actions.dispatch('refreshFolders', true);
                                props.actions.dispatch('openDialog', {
                                    message: `A(z) ${item.post_title} című poszt bekerült a(z) ${dropResult.name} könyvtárba!`,
                                    variant: 'success',
                                    opened: true
                                });
                            });
                        }
                    } else { //mar rendelkezik, szolunk a usernek
                        props.actions.dispatch('openDialog', {
                            message: `A kiválasztott poszt már szerepel ebben a könyvtárban!`,
                            variant: 'error',
                            opened: true
                        });
                    }

                }
            }
        },
        collect: monitor => ({
            isDragging: monitor.isDragging(),
        }),
    })
    const opacity = isDragging ? 0.4 : 1;
    const isFolder = props.tabPath.indexOf('folder-') !== -1;
    let dom;
    let content = (<div className="MuiListItem-wrapper">
                    {
                        !isFolder
                        ? <IconButton
                            aria-label="more"
                            aria-controls="long-menu"
                            aria-haspopup="true"
                            data-item-id={props.item.post_ID}
                            onClick={props.openModalEditor}
                            >
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 13.616v-3.232c-1.651-.587-2.694-.752-3.219-2.019v-.001c-.527-1.271.1-2.134.847-3.707l-2.285-2.285c-1.561.742-2.433 1.375-3.707.847h-.001c-1.269-.526-1.435-1.576-2.019-3.219h-3.232c-.582 1.635-.749 2.692-2.019 3.219h-.001c-1.271.528-2.132-.098-3.707-.847l-2.285 2.285c.745 1.568 1.375 2.434.847 3.707-.527 1.271-1.584 1.438-3.219 2.02v3.232c1.632.58 2.692.749 3.219 2.019.53 1.282-.114 2.166-.847 3.707l2.285 2.286c1.562-.743 2.434-1.375 3.707-.847h.001c1.27.526 1.436 1.579 2.019 3.219h3.232c.582-1.636.75-2.69 2.027-3.222h.001c1.262-.524 2.12.101 3.698.851l2.285-2.286c-.744-1.563-1.375-2.433-.848-3.706.527-1.271 1.588-1.44 3.221-2.021zm-12 2.384c-2.209 0-4-1.791-4-4s1.791-4 4-4 4 1.791 4 4-1.791 4-4 4z"/></svg>
                            </IconButton>

                        : null
                    }

                    {
                        isFolder
                        ? <IconButton
                            data-folder-id={window.location.pathname.split('/').slice(-1)[0]}
                            data-item-id={props.item.post_ID}
                            data-blog-id={props.item.post_blog_ID}
                            onClick={props.removeFolder}
                            >
                                <DeleteIcon />
                            </IconButton>

                        : null    
                    }

                    {props.displayList(props.tabPath, props.item)}
                    </div>);
    
    dom = <ListItem className="hoverList" ref={drag} style={{opacity}}>
            {content}
          </ListItem>;

    return dom;
}

export default DraggableListItem;