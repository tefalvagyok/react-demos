import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';
import Switch from '@material-ui/core/Switch';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

export default class ModalFolderEditor extends React.Component {
	constructor(props) {
	 	super(props);

	 	this.state = {
	 		folders: this.props.folders, //mappak
	 		folderId: this.props.folderId, //mappa azonosito
	 		folderName: "", //mappa neve
	 		folderShared: false, //a mappa megosztott-e (igen / nem)
	 		saved: false //mentve (igen / nem)
	 	};

	 	this.save 				 = this.save.bind(this);	 	
	 	this.getFolderById 		 = this.getFolderById.bind(this);
	 	this.inputChangeHandler  = this.inputChangeHandler.bind(this);
	 	this.getFolderData 		 = this.getFolderData.bind(this);
	}


	/**
	 * itt minden lefut, amint a komponens mountolodott
	 */
	componentDidMount() {
		if (this.state.folderId !== undefined) {
			this.getFolderData(this.state.folderId);
		}
	}


    /**
     * input esemenykezelo
     * ha a key nem folderShared, akkor az inputunk nem radio, a state erteke = a dom object ertekevel
     * ha a key folderShared, akkor az inputunk radio
     * ----------------------        
     * @param {string} key:         a state neve, amelyet frissiteni akarunk
     * @param {string} target:      a dom object, amelyen az esemeny lefut
     */
	inputChangeHandler = (key, target) => {
		if (key !== 'folderShared') {
			this.setState({ [key]: target.value });
			target.value = this.state[key];
		} else {
			this.setState({ [key]: target.checked }, () => console.log(this.state[key]));
			target.checked = this.state[key];
		}
	}


    /**
     * visszaad egy custom filtert a stateben tarolt filterek kozul id alapjan
     * ----------------------        
     * @param {integer} id:         a custom filter azonositoja
     */
	getFolderById = (id) => {
		return this.state.folders.find((element) => {
            return element.id === id;
        })
	}


    /**
     * befrissiti az urlap inputok ertekeit custom filter azonosito alapjan (ha letezik)
     * ----------------------        
     * @param {integer} id:         a custom filter azonositoja
     */
	getFolderData = (id) => {
		let selectedFolder = this.getFolderById(id);

		if (selectedFolder !== undefined) {
			this.setState({
				folderName: selectedFolder.name,
				folderShared: selectedFolder.shared 
			});
		} else {
			return false;
		}
	}


    /**
     * a mentes gombra kattintva fut le, osszegyujti az urlap inputok ertekeit, 
     * es ha nincs customFilterId, akkor letrehozunk a db-ben egy uj rekordot,
     * ha van customFilterId, updateli a db-ben a custom filterunket, 
     * vegul triggereli az oldalsavi szurok frissiteset
     */
	save = () => {
		let self 			= this;
		let folderId  		= this.state.folderId;
		let folderShared	= this.state.folderShared;
		let folderName 	    = document.querySelector('input[name="name"]').value;		

		if (folderId === null) { //nincs folderId, create request
			self.props.action("folder/create", "POST", "name=" + folderName + "&shared=" + Number(folderShared), function() {
	            self.props.actions.dispatch('refreshFolders', true);
	            self.props.actions.dispatch('openDialog', {
	            	message: `Mappa létrehozva`,
	            	variant: 'success',
	            	opened: true
	            });
	        });			
		} else { //van folderId, update request
	        self.props.action("folder/update?id=" + folderId, "PUT", "name=" + folderName + "&shared=" + Number(folderShared), function() {
	            self.props.actions.dispatch('refreshFolders', true);
	            self.props.actions.dispatch('openDialog', {
	            	message: `Mappa frissítve`,
	            	variant: 'success',
	            	opened: true
	            });
	        });			
		}
	}


    /** 	 
 	 * itt rendereljuk ki a modal folder editor komponensunk templatejet
	 */
	render() {		
		let { folderName, folderShared } = this.state;

		return (
			<Grid container spacing={3}>				
				<Grid item xs={12}>
					<TextField
					id="name"
					name="name"
					fullWidth
					className={this.props.classes.textField}
					label="Mappa neve"
					margin="normal"
					value={folderName}					
					onChange={e => this.inputChangeHandler('folderName', e.target)}
					/>
             	</Grid>

             	<Grid item xs={12}>
             	<FormControlLabel
             	control={
             		<Switch
             		checked={Boolean(folderShared)}
             		onChange={e => this.inputChangeHandler('folderShared', e.target)}
             		value="1"
             		color="primary"
             		/>
             	}
             	label="Megosztott"
             	/>
             	</Grid>

				<Grid item xs={12}>
	             	<Button variant="contained" color="primary" className={this.props.classes.button} onClick={this.save}>
	             		Mentés
	             	</Button>
             	</Grid>
             </Grid>
		);
	}
}