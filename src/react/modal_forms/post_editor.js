import React from 'react';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import Divider from '@material-ui/core/Divider';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';

//elvalaszto csik + cimke ikon
const TagIcon = () => {
	return (
		<span>
			<Divider className="hr" />
			<i className={['fa','fa-tags'].join(' ')}></i>			
		</span>
	);
};

export default class ModalPostEditor extends React.Component {
	constructor(props) {
	 	super(props);

	 	this.state = {
	 		item: this.props.item, //betoltott poszt
	 		folders: this.props.folders, //konyvtarak
	 		loadedItems: this.props.loadedItems, //betoltott posztok
	 		openedModalPostId: this.props.openedModalPostId, //aktualis poszt id
			allSalestags: this.props.allSalestags, //salestagek
	 	};

	 	this.salesTagHandleChange = this.salesTagHandleChange.bind(this);
	 	this.checkKey = this.checkKey.bind(this);
	}


	/**
	 * itt minden lefut, amint a komponens mountolodott
	 */
	componentDidMount() {
		let { loadedItems } = this.state;

		//osszes poszt frissitese
		this.props.actions.subscribe('loadedItems', (data) => {
        	this.setState({ 
        		loadedItems: data
        	});
        });

		//modalban megnyitott posztok befrissitese
		this.props.actions.subscribe('modalPostIdChanged', (data) => {
			let loadedItem = this.props.findArrayElementById(loadedItems, 'post_ID', data);

			if (loadedItem !== undefined) {
				this.setState({
					item: loadedItem,
					openedModalPostId: data
				});				
			}
		});

        //meghivjuk a fuggvenyt gombnyomasok figyelesere
        document.onkeydown = this.checkKey;
	}


    /**
     * itt minden lefut, amint a komponens frissul
     */
    componentDidUpdate(prevProps, prevState) {
    	//item befrissitese
		if (this.props.item !== prevProps.item) {
			this.setState({
				item: this.props.item
			});
		}

		//loaded items befrissitese
		if (this.props.loadedItems !== prevProps.loadedItems) {
			this.setState({
				loadedItems: this.props.loadedItems
			});
		}
    }


    /**
 	 * kiszedi az author_url-bol a user id-t
 	 * ----------------------    	 
	 * @param {string} url: a user id-t tartalmazo url
	 * @return {integer}: 	numerikus user id
	 */ 
    getUserId(url) {
    	return url.match(/\d+/)[0];
    }


    /**
     * gombnyomasra triggerel kulonbozo fuggvenyeket
     * ----------------------        
     * @param {object} event: gombnyomas esemeny objektuma
     */ 
	checkKey(event) {		
		let { item, openedModalPostId } = this.state;

        if (openedModalPostId !== undefined) {
        	switch(event.keyCode) {
        		
        		//balra gomb
        		case 37: 
                this.props.prevPost();
                break;

                //jobbra gomb
                case 39: 
                this.props.nextPost();
                break;

                //v gomb
                case 86: 
                window.open(item.url);
                break;

                //a gomb
                case 65: 
                window.open(`https://blog.hu/user/${this.getUserId(item.author_url)}/tab/data`);
                break;

                //c gomb
                case 67: 
                window.open(`https://blog.hu/user/${this.getUserId(item.author_url)}/tab/msg`);
                break;

                default:
                // console.log(event.keyCode);
                break;
        	}
        }
    }


    /**
     * salestagek ki / bekapcsolasa (single)
     * befrissiti a state-ben tarolt selectedSalestags objektumot
     * ----------------------        
     * @param {object} event: kattintas esemeny objektuma
     * @param {string} index: salestag indexe
     */ 
    salesTagHandleChange = (blogId, event) => {
    	let { selectedSalestags, allSalestags } = this.state;
        let salestagId = parseInt(this.props.findArrayElementById(allSalestags, 'salestag_tag', Object.values(selectedSalestags)[0]).salestag_ID);

        this.props.action("post-list/remove-salestag?id=" + blogId + "&salestag_id=" + salestagId, "DELETE", null, function() {
            console.log('törölve')
            
            this.props.action("post-list/add-salestag?id=" + blogId + "&salestag_id=" + event.target.value, "PUT", null, function() {
	            console.log('hozzáadva')
	        });
        });

        this.setState({
            selectedSalestags: [event.target.value]
        }, () => {
            console.log(selectedSalestags)
        })
    }


    /**      
     * itt rendereljuk ki a poszt szerkeszto modal ablakot
     */
	render() {
		let template;
		let { item, allSalestags } = this.state;
		let { classes } = this.props;

		if (this.state.item) {
			let pubDateObj 	= new Date(this.state.item.post_datestart); 
			let pubDate 	= pubDateObj.toLocaleDateString('hu', {
				weekday: "long",
				year: "numeric",
				month: "long",
				day: "numeric"
			});
		
			template = <div>
				<div className="modal-post-wrapper">
					<div id="page-wrapper">
						<div id="page-top-wrapper">
				            <div id="page-details">
				            	<a rel="noopener noreferrer" target="_blank" href={this.state.item.blog_url}>{this.state.item.blog_name}</a> - {pubDate} by&nbsp;							            	
				            	<a rel="noopener noreferrer" target="_blank" href={`https://blog.hu/user/${this.getUserId(this.state.item.author_url)}/tab/data`}>{this.state.item.blog_name}</a>
				            	<a rel="noopener noreferrer" target="_blank" href={`https://blog.hu/user/${this.getUserId(this.state.item.author_url)}/tab/msg`}>&nbsp;
				            		<i className={['fa','fa-envelope'].join(' ')}></i>
		            			</a>
		            			<a rel="noopener noreferrer" target="_blank" href={`${this.state.item.blog_url}/admin/post/edit/${this.state.item.post_ID}`}>&nbsp;
				            		<i className={['fa','fa-pencil'].join(' ')}></i>
		            			</a>
				            </div>
						</div>
							
						<div id="page-content" dangerouslySetInnerHTML={{__html: this.state.item.post_content}}></div>
					</div>
				</div>


				{/* TAGS */}
				<ExpansionPanel>
	                <ExpansionPanelSummary
	                expandIcon={<ExpandMoreIcon />}
	                aria-controls="panel1a-content"
	                id="panel1a-header"
	                >
	                    <Typography className={[classes.heading, 'expansion-panel-heading'].join(' ')}>Tagek</Typography>
	                </ExpansionPanelSummary>
	                <ExpansionPanelDetails>
	                    <FormControl className={[classes.formControl, 'mocsok'].join(' ')}>
	                        <InputLabel htmlFor="select-multiple-chip">Salestagek</InputLabel>
	                        <Select
                            value={Object.values(this.state.item.salestags)}
                            onChange={this.salesTagHandleChange.bind(null, this.state.item.post_blog_ID)}
                            inputProps={{
                                name: 'salestags',
                                id: 'select-salestag',
                            }}
                            >
                                {
                                    allSalestags.map((item, key) => 
                                        (item.salestag_tag === "Salestagek" 
                                        ? <MenuItem value="" disabled>Salestagek</MenuItem>
                                        : <MenuItem key={key} value={item.salestag_tag}>{item.salestag_tag}</MenuItem>)
                                    )
                                } 
                            </Select>
	                    </FormControl>

	                    <div className="mocsok">
	                        <i className={['fa','fa-tags'].join(' ')}></i>

	                        {this.props.initBlogTagChip('a:noszerkfeed', this.state.item.blog_tags)}
	                        {this.props.initBlogTagChip('a:copycat', this.state.item.blog_tags)}
	                        {this.props.initBlogTagChip('a:szponzorblog', this.state.item.blog_tags)}
	                        {this.props.initBlogTagChip('a:holdudvar', this.state.item.blog_tags)}
	                    </div>
	                </ExpansionPanelDetails>
	            </ExpansionPanel>
				{/* TAGS */}
	            

				{/* BLOG */}
                <ExpansionPanel>
                    <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                    >
                        <Typography className={[classes.heading, 'expansion-panel-heading'].join(' ')}>Blog</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <div className="expansion-panel-cell">
                            <b><i className={['fa','fa-info-circle'].join(' ')}></i> Státusz:</b> {item.blog_adstatus !== undefined ? item.blog_adstatus : null}<br />
                            <b><i className={['fa','fa-eye'].join(' ')}></i> PI:</b> {item.blog_pi !== undefined ? item.blog_pi : null}<br />
                            <b><i className={['fa','fa-users'].join(' ')}></i> UV:</b> {item.blog_uv !== undefined ? item.blog_uv : null}

                            {
                            	item.blog_tags !== undefined
                            	? item.blog_tags.length > 0 ? <TagIcon /> : null
                            	: null
                            }

                            {item.blog_tags.length > 0
								? item.blog_tags.map((tag, index) => (
									<Chip label={tag} key={index} className={classes.chip} /> 
								))
								: null
							}
                        </div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                {/* BLOG */}


            	{/* POST */}
                <ExpansionPanel>
                    <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                    >
                        <Typography className={[classes.heading, 'expansion-panel-heading'].join(' ')}>Post</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <div className="expansion-panel-cell">
                            <b><i className={['fa','fa-comments'].join(' ')}></i> <a rel="noopener noreferrer" target="_blank" href={`${item.url}#comments`}>Kommentek</a>:</b> {item.comment_count}
                        
                            {item.post_tags.length > 0 ? <TagIcon /> : ''}

							{item.post_tags.length > 0
								? item.post_tags.map((tag, index) => (
									<Chip label={tag} key={index} className={classes.chip} /> 
								))
								: null
							}
                        </div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            	{/* POST */}


           		{/* META */}
           		{
           			item.reader_meta !== null && item.reader_meta !== undefined
           			? <ExpansionPanel>
		                    <ExpansionPanelSummary
		                    expandIcon={<ExpandMoreIcon />}
		                    aria-controls="panel2a-content"
		                    id="panel2a-header"
		                    >
		                        <Typography className={[classes.heading, 'expansion-panel-heading'].join(' ')}>Meta</Typography>
		                    </ExpansionPanelSummary>
		                    <ExpansionPanelDetails>
		                        <div className="expansion-panel-cell">
		                        	<b><i className={['fa','fa-picture-o'].join(' ')}></i> Kép: </b> 
		                        	{
		                        		item.reader_meta.image !== undefined 
		                        		? <a rel="noopener noreferrer" className="meta-image" target="_blank" href={item.reader_meta.image}>{item.reader_meta.image}</a>
		                        		: null
		                        	}<br />

		                        	<b><i className={['fa','fa-font'].join(' ')}></i> Cím: </b> 
		                        	{
		                        		item.reader_meta.title !== undefined 
		                        		? item.reader_meta.title
		                        		: null
		                        	}<br />

		                        	<b><i className={['fa','fa-align-justify'].join(' ')}></i> Lead: </b> 
		                        	{
		                        		item.reader_meta.lead !== undefined 
		                        		? item.reader_meta.lead
		                        		: null
		                        	}
		                        </div>
		                    </ExpansionPanelDetails>
		                </ExpansionPanel>
           			: null
           		}                
            	{/* META */}
            </div>;	
		}

		return template;
	}
}