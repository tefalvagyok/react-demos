import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormGroup from '@material-ui/core/FormGroup';
import FormLabel from '@material-ui/core/FormLabel';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Radio from '@material-ui/core/Radio';
import Grid from '@material-ui/core/Grid';

export default class ModalFilterEditor extends React.Component {
	constructor(props) {
	 	super(props);

	 	this.state = {
	 		filters: this.props.filters, //szurok
	 		customFilterId: this.props.customFilterId, //sajat szuro azonosito
	 		filterName: "", //szuro neve
	 		tagType: "", //szuro tipusa
	 		tags: "", //szuro tagjei
	 		adstatus: [], //szuro adstatusa
	 		saved: false //mentve (igen / nem)
	 	};

	 	this.save 				 = this.save.bind(this);
	 	this.getFilterById 		 = this.getFilterById.bind(this);
	 	this.inputChangeHandler  = this.inputChangeHandler.bind(this);
	 	this.getCustomFilterData = this.getCustomFilterData.bind(this);
	}


	/**
	 * itt minden lefut, amint a komponens mountolodott
	 */
	componentDidMount() {
		if (this.state.customFilterId !== undefined) {
			this.getCustomFilterData(this.state.customFilterId);
		}
	}


    /**
     * input esemenykezelo
     * ha a key nem adstatus, akkor az inputunk nem checkbox, a state erteke = a dom object ertekevel
     * ha a key adstatus, akkor az inputunk checkbox es kulon vizsgalat kell a bepipalt uj ertekek osszehasonlitasara
     * ----------------------        
     * @param {string} key:         a state neve, amelyet frissiteni akarunk
     * @param {string} target:      a dom object, amelyen az esemeny lefut
     */
	inputChangeHandler = (key, target) => {
		if (key !== "adstatus") {
			this.setState({ [key]: target.value });
			target.value = this.state[key];
		} else {
            let array	= this.state.adstatus;
            let check 	= target.checked;
            let checked = target.value.toLowerCase();            
        
            if (check === true) {
                this.setState(prevState => ({
	                adstatus: [...prevState.adstatus].concat([checked])
	            }));
            } else {
                var index = array.indexOf(checked);

                if (index > -1) {
                    array.splice(index, 1);

                    this.setState({
                        adstatus: array
                    })
                } 
            }
		}
	}


    /**
     * visszaad egy custom filtert a stateben tarolt filterek kozul id alapjan
     * ----------------------        
     * @param {integer} id:         a custom filter azonositoja
     */
	getFilterById = (id) => {
		return this.state.filters.find((element) => {
            return element.id === id;
        })
	}


    /**
     * befrissiti az urlap inputok ertekeit custom filter azonosito alapjan (ha letezik)
     * ----------------------        
     * @param {integer} id:         a custom filter azonositoja
     */
	getCustomFilterData = (id) => {
		let selectedFilter = this.getFilterById(id);

		if (selectedFilter !== undefined) {
			let selectedFilterDatas = JSON.parse(selectedFilter.params);

			this.setState({
				filterName: selectedFilterDatas.title,
				tagType: selectedFilterDatas.tag_type,
				tags: Array.isArray(selectedFilterDatas.tags) ? selectedFilterDatas.tags.join(", ") : selectedFilterDatas.tags,
				adstatus: selectedFilterDatas.adstatus
			});
		} else {
			return false;
		}
	}


    /**
     * visszaad egy tombot a bepipalt hirdetesi statuszok ertekeivel
     * ----------------------        
     * @return {array}:         becheckelt hirdetesi statuszok tombje
     */
	getAdStatus = () => {
		let ret = [];
		let adstatusArr = document.querySelectorAll('input[name^="adstatus"]');
		
		for (var i = 0 ; i < adstatusArr.length; i++) {
			if (adstatusArr[i].checked === true) {
				ret.push(adstatusArr[i].value.toLowerCase());
			}
		}

		return ret;
	}


    /**
     * megvizsgalja, hogy egy adstatus azonosito szerepel-e mar a state-ben tarolt adstatus azonositok kozott (ha van customFilterId)
     * ----------------------        
     * @param {integer} id:       adstatus azonositoja
     * @return {boolean}:         szerepel / nem szerepel
     */
	hasAdStatus = (id) => {
		let customFilterId = this.state.customFilterId;

		if (!isNaN(customFilterId)) {
			if (Array.isArray(this.state.adstatus)) {
				return this.state.adstatus.includes(id);
			} else {
				return this.state.adstatus === id;
			}
		} else {
			return false;
		}
	}


    /**
     * a mentes gombra kattintva fut le, osszegyujti az urlap inputok ertekeit, 
     * es ha nincs customFilterId, akkor letrehozunk a db-ben egy uj rekordot,
     * ha van customFilterId, updateli a db-ben a custom filterunket, 
     * vegul triggereli az oldalsavi szurok frissiteset
     */
	save = () => {
		let self 			= this;
		let customFilterId  = this.state.customFilterId;
		let filterName 	    = document.querySelector('input[name="title"]').value;
		let tagType    		= document.querySelector('.radio-wrapper .Mui-checked input[type="radio"]').value;
		let tags 	   		= document.querySelector('input[name="tags"]').value;
		let adStatus   		= this.getAdStatus();
		let params	   		= {
			title: filterName,
			tag_type: tagType,
			tags: tags,
			adstatus: adStatus,
			// user_id:
		};

		if (customFilterId === null) { //nincs customFilterId, create request
			//erre még rá kell nézni!!
			//ez most egyelore csak azert mukodik, mert \reader\models\Filters rules()-ban kikommenteztuk a user_id required-et
			var createRequest = new Request("/reader/filter/create", {
				method: "POST",
				headers: { 
					"Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
				},
				body: "params=" + encodeURIComponent(JSON.stringify(params))
			});
		} else { //van customFilterId, update request
			var putRequest = new Request("/reader/filter/update?id=" + customFilterId, {
				method: "PUT",
				headers: { 
					"Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
				},
				body: "params=" + encodeURIComponent(JSON.stringify(params))
			});
		}
		
		var request = fetch( customFilterId === null ? createRequest : putRequest);

		request.finally(() => {
     		setTimeout(function() {
	     		self.props.actions.dispatch('refreshFilters', true);
	     		self.props.actions.dispatch('openDialog', {
	            	message: (customFilterId === null ? 'Szűrő létrehozva' : 'Szűrő frissítve'),
	            	variant: 'success',
	            	opened: true
	            });
     		}, 1000);
     	});
	}


    /** 	 
 	 * itt rendereljuk ki a modal filter editor komponensunk templatejet
	 */
	render() {		
		let { filterName, tagType, tags } = this.state;

		return (
			<Grid container spacing={3}>				
				<Grid item xs={12}>
					<TextField
					id="title"
					name="title"
					fullWidth
					className={this.props.classes.textField}
					label="Szűrő neve"
					margin="normal"
					value={filterName}					
					onChange={e => this.inputChangeHandler('filterName', e.target)}
					/>
             	</Grid>

				<Grid item xs={12}>
					<FormControl fullWidth component="fieldset" className={[this.props.classes.formControl, 'radio-wrapper'].join(' ')}>
						<FormLabel component="legend">Címkék:</FormLabel>
	                    <RadioGroup value={tagType} name="tag_type" style={{ flexDirection: 'unset' }} onChange={e => this.inputChangeHandler('tagType', e.target)}>
		                    <FormControlLabel key="0" value="blog" control={<Radio />} label="Blog" style={{ display: 'inline-flex', width: '20%' }} />
	                        <FormControlLabel key="1" value="item" control={<Radio />} label="Poszt" style={{ display: 'inline-flex', width: '20%' }} />
	                    </RadioGroup>
	                </FormControl>
             	</Grid>

				<Grid item xs={12}>
					<TextField
					id="tags"
					name="tags"
					fullWidth
					className={this.props.classes.textField}
					label="Címkék vesszővel elválasztva"
					margin="normal"
					value={tags}
					onChange={e => this.inputChangeHandler('tags', e.target)}
					/>
             	</Grid>

				<Grid item xs={12}>
					<FormControl fullWidth component="fieldset" className={this.props.classes.formControl}>
						<FormLabel component="legend">Hirdetési státusz:</FormLabel>
						<FormGroup style={{ flexDirection: 'unset' }}>
							{['A','B','C','D'].map((item, key) => (								
			                	<FormControlLabel 
			                	label={item} 
			                	key={key}
			                	control={<Checkbox className="adstatus" name={`adstatus[${item.toLowerCase()}]`} 
			                	checked={this.hasAdStatus(item.toLowerCase()) !== false ? true : false} value={item} />} 
			                	style={{ display: 'inline-flex', width: '10%' }}
			                	onChange={e => this.inputChangeHandler('adstatus', e.target)}
			                	/>
							))}			                
		                </FormGroup>
	                </FormControl>
             	</Grid>

				<Grid item xs={12}>
	             	<Button variant="contained" color="primary" className={this.props.classes.button} onClick={this.save}>
	             		Mentés
	             	</Button>
             	</Grid>
             </Grid>
		);
	}
}