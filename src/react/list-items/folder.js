import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import Badge from '@material-ui/core/Badge';
import { useDrop } from 'react-dnd';

//szines jelvenyek (konyvtarakra)
const StyledBadge = withStyles(theme => ({
	badge: {
		right: -3,
		border: `2px solid ${theme.palette.background.paper}`,
		padding: '0 4px',
	},
}))(Badge);


//fodrozodas szine
const styles = { 
    buttonRipple: { 
        color: "#f47629" 
    },
    touchRipple: {
        color: "#f47629"         
    }
};

//d&d tipus
export const ItemTypes = {
  BOX: 'box'
};


/**
 * mappa lista elem komponens
 * betolti a mappa lista elem ikonjat, szoveget es a mappa elemeinek szamat (kattintaskor narancs fodrozodassal)
 * draggelheto lista elemeket fogad, ha rahuzzuk
 */
const Folder = (props) => {
	const [{ canDrop, isOver }, drop] = useDrop({
		accept: ItemTypes.BOX,
		drop: () => ({ 
			name: props.name, 
			folder_id:  props.id
		}),
		collect: monitor => ({
			isOver: monitor.isOver(),
			canDrop: monitor.canDrop(),
		}),
	});
	const isActive = canDrop && isOver;
	let backgroundColor = 'transparent';

	if (isActive) {
		backgroundColor = 'rgba(244,118,41,0.5)';
	} else if (canDrop) {
		backgroundColor = 'rgba(244,118,41,0.1)';
	}

	return (
		<ListItem 
		button 
		key={props.id} 
		ref={drop}		
		style={{ backgroundColor: backgroundColor }}
		TouchRippleProps={{ classes: { root: props.classes.buttonRipple } }} 
		>
			<StyledBadge badgeContent={props.badgeContent} color="primary">
				<i className={['fa', 'fa-folder'].join(" ")} aria-hidden="true"></i>
			</StyledBadge>

			<ListItemText primary={props.name} />
		</ListItem>
	)
};

export default withStyles(styles)(Folder);