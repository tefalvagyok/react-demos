import React from 'react'
import { withStyles } from '@material-ui/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

//fodrozodas szine
const styles = { 
    buttonRipple: { 
        color: "#f47629" 
    },
    touchRipple: {
        color: "#f47629"         
    }
};


/**
 * szuro lista elem komponens
 * betolti a szuro lista elem ikonjat, szoveget (kattintaskor narancs fodrozodassal)
 */
const FilterListItem = (props) => {
    return(
        <ListItem 
        button 
        key={props.index}
        TouchRippleProps={{ classes: { root: props.classes.buttonRipple } }} 
        >
            <i key={props.index} className={['fa', props.LoadIcon( isNaN(props.item.id) ? props.item.id : 'custom' + props.item.id ).iconClass].join(" ")} />
            <ListItemText primary={(typeof props.item.id !== "number" ? props.item.params.title : JSON.parse(props.item.params).title)} />
        </ListItem>
    );
}

export default withStyles(styles)(FilterListItem);