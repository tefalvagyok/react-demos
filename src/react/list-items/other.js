import React from 'react'
import { withStyles } from '@material-ui/styles';
import Link from '@material-ui/core/Link';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

//fodrozodas szine
const styles = { 
    buttonRipple: { 
        color: "#f47629" 
    },
    touchRipple: {
        color: "#f47629"         
    }
};

/**
 * egyeb lista elem komponens (pl.: szurok / mappak letrehozas gombjaihoz, lenyithato menupontokhoz)
 * betolti a lista elem ikonjat, szoveget (kattintaskor narancs fodrozodassal)
 * lenyithato menupontok megjelenitesere is kepes
 */
const OtherListItem = (props) => {
    let text;

    if (props.collapseFiltersOpen !== undefined) {
        text = <ListItemText primary={props.title} className={['collapseFiltersBtn', (props.collapseFiltersOpen !== false ? "pull-left" : null)].join(" ")} />;
    } else {
        text = <ListItemText primary={props.title} />;
    }
            
    return(
        <ListItem 
        button 
        key={props.index}
        className="otherSidebarBtn"
        TouchRippleProps={{ classes: { root: props.classes.buttonRipple } }} 
        onClick={props.onClickFunction}
        >
            <i className={['fa', props.iconName].join(" ")} aria-hidden="true"></i>

            {props.collapseFiltersOpen !== undefined
                ? <Link to="#">
                    {text}

                    <ListItemIcon className={['pull-right', 'collapseIcon'].join(' ')}>
                        {props.collapseFiltersOpen === true ? <ExpandLess /> : <ExpandMore />}
                    </ListItemIcon>
                  </Link>
                : text
            }
        </ListItem>
    );
}

export default withStyles(styles)(OtherListItem);