//importalt komponensek
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import IconButton from '@material-ui/core/IconButton';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import ListItemText from '@material-ui/core/ListItemText';
import { Link as RouterLink } from 'react-router-dom'
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import PropTypes from 'prop-types';

import ModalPostEditor from './react/modal_forms/post_editor';
import ModalFilterEditor from './react/modal_forms/filter_editor';
import ModalFolderEditor from './react/modal_forms/folder_editor';
import DraggableListItem from './draggable-list-item';
import ModalComponent from './modal';
import Loading from './loading';


//material ui styles
const classes = makeStyles(theme => ({
    root: {
        width: '100%',
        maxWidth: 360,        
        backgroundColor: theme.palette.background.paper,
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: '100%',
        maxWidth: 300,
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: 2,
    }
}));



export default class ListComponent extends React.Component {

    /**
     * a constructor state-jeben tarolunk minden erteket, 
     * amelyre a komponensunknek szuksege lesz
     */
    constructor(props) {
        super(props);
        this.state = {
            version: this.props.version, //valtozat (dev / prod)

            loadedItems: [], //betoltott itemek 
            page: 1, //betoltott elemek oldalszama

            filters: this.props.filters, //szurok
            folders: this.props.folders, //mappak

            openedModal: false, //modal nyitva / zarva
            openedModalTitle: "", //modal fejlec cim
            openedModalPostId: null, //modal poszt azonosito

            openedModalFilterEditor: false, //modal szuro szerkeszto nyitva / zarva
            openedModalFolderEditor: false, //modal mappa szerkeszto nyitva / zarva

            customFilterId: this.props.customFilterId, //sajat szuro azonosito
            folderId: this.props.folderId, //mappa azonosito
            
            selectedFolderId: [], //kivalasztott konyvtar(ak)
            openedTabs: this.props.openedTabs, //megnyitott tabok
            tabIndex: this.props.tabIndex, //aktualis tab indexe
            tabPath: this.props.tabPath, //aktualis tab utvonala
            allSalestags: this.props.allSalestags, //osszes salestag
            selectedSalestags: [], //kivalasztott salestagek
            loading: this.props.loading //toltes igen / nem
        };
        
        this.loadTab = this.loadTab.bind(this);        
        this.flagPostUnseen = this.flagPostUnseen.bind(this);
        this.trackScrolling = this.trackScrolling.bind(this);
        this.findArrayElementById = this.findArrayElementById.bind(this);
        this.getPostIndex = this.getPostIndex.bind(this);
        this.nextPost = this.nextPost.bind(this);
        this.prevPost = this.prevPost.bind(this);
    }
    

    /**
     * triggereljuk a lista komponens unmountolasat
     */
    dismiss() {
        this.props.handleUnmount('list');
    } 


    /**
     * itt minden lefut, amint a komponens mountolodott
     */
    componentDidMount() {

        //osszes poszt frissitese
        this.props.actions.subscribe('loadedItems', (data) => {
            this.setState({ 
                loadedItems: data
            });
        });
        
        //uj filter letrehozasa
        this.props.actions.subscribe('addCustomFilter', (data) => {
            this.setState({
                openedModal: true,
                openedModalFilterEditor: true,
                openedModalTitle: "Új szűrő létrehozása"
            });
        });

        //uj folder letrehozasa
        this.props.actions.subscribe('addFolder', (data) => {
            this.setState({
                openedModal: true,
                openedModalFolderEditor: true,
                openedModalTitle: "Új mappa létrehozása"
            });
        });

        //filter szerkesztese
        this.props.actions.subscribe('editCustomFilter', (data) => {
            this.setState({
                openedModal: true,
                openedModalFilterEditor: true,
                openedModalTitle: "Szűrő szerkesztése",
                customFilterId: this.props.customFilterId
            });
        });

        //folder szerkesztese
        this.props.actions.subscribe('editFolder', (data) => {
            this.setState({
                openedModal: true,
                openedModalFolderEditor: true,
                openedModalTitle: "Mappa szerkesztése",
                folderId: this.props.folderId
            });
        });

        //tabok befrissitese
        this.props.actions.subscribe('refreshTabs', (data) => {
            this.loadTab();
        });

        //bezarjuk a modal editort, ha a szurok befrissultek
        this.props.actions.subscribe('refreshFilters', (data) => {
            this.closeModalEditor();
        });

        //bezarjuk a modal editort, ha a mappak befrissultek
        this.props.actions.subscribe('refreshFolders', (data) => {
            this.closeModalEditor();
        });

        //betoltjuk az aktualis tabot      
        this.loadTab();
    }


    /**
     * itt minden lefut, amint a komponens frissul
     */
    componentDidUpdate(prevProps, prevState) {
        let { loadedItems, openedModal, openedModalPostId, selectedSalestags } = this.state;

        //osszes poszt frissitese
        this.props.actions.subscribe('loadedItems', (data) => {
            if (data !== prevState.loadedItems) {
                this.setState({ 
                    loadedItems: data
                });
            }
        });

        //post-list frissitese
        this.props.actions.subscribe('refreshPostList', (data) => {
            if (data !== prevState.loadedItems) {
                this.setState({
                    loadedItems: data
                });
            }
        });

        //filterek frissitese
        this.props.actions.subscribe('refreshedFilters', (data) => {
            if (data !== prevState.filters) {
                this.setState({
                    filters: data
                });
            }
        });

        //folderek frissitese
        this.props.actions.subscribe('refreshedFolders', (data) => {
            if (data !== prevState.folders) {
                this.setState({
                    folders: data
                });
            }
        });

        if (loadedItems.length) { //ha a lista elemek betoltodtek
            if (prevState.openedModal !== openedModal && prevState.openedModalPostId !== openedModalPostId) {

                if (openedModalPostId) {
                    let item = this.findArrayElementById(loadedItems, 'post_ID', openedModalPostId);                
                    
                    if (!selectedSalestags.length) { //ha van kivalasztott salestag
                        this.setState({ //frissitjuk a kivalasztott salestag state-et
                            selectedSalestags: item.salestags
                        })
                    }
                }
            }
        }
    }


    /**
     * itt minden lefut, amint a komponens unmountolodik
     */
    componentWillUnmount() {
        document.removeEventListener('scroll', this.trackScrolling);

        // this.props.actions.retain('viewType');
        this.props.actions.retain('addCustomFilter');
        this.props.actions.retain('editCustomFilter');
        this.props.actions.retain('deleteCustomFilter');

        this.props.actions.retain('addFolder');
        this.props.actions.retain('editFolder');
        this.props.actions.retain('deleteFolder');

        this.props.actions.retain('loadedItems');
        this.props.actions.retain('openedTabs');         
        this.props.actions.retain('tabIndex');         
    }


    /**
     * megvizsgaljuk, hogy a megadott dom element elerte-e az oldal aljat
     * ----------------------        
     * @param {object} element:     vizsgalt dom object
     * @return {boolean}:           elerte az oldal aljat? (true / false)
     */
    isBottom(element) {
        return element.getBoundingClientRect().bottom <= (window.innerHeight - 25); //25px az also margin
    }


    /**
     * beallitja a fetch url-eket verziotol fuggoen
     */
    getFetchUrl = (withPage = false) => {
        let url;
        let { version, page, openedTabs, tabPath } = this.state;
        let activeTab = this.findArrayElementById(openedTabs, 'id', tabPath);
        let nextPage  = (withPage === true ? '&page=' + (page + 1) : '');

        if (activeTab.id.indexOf('folder-') === -1) {
            if (activeTab.id.indexOf('custom') !== -1) {
                url = (version === "dev" ? "./json/all.json" : "/reader/post-list/filter?id=" + activeTab.id.split('custom')[1] + "&limit=100" + nextPage);
            } else {
                url = (version === "dev" ? "./json/" + activeTab.id + ".json" : "/reader/post-list/filter?id=" + activeTab.id + "&limit=100" + nextPage);
            }
        } else {
            url = (version === "dev" ? "./json/folder-69.json" : "/reader/post-list/folder?id=" + activeTab.id.split('-')[1] + "&limit=100");
        }

        return url;
    }


    /**
     * ha elertuk az oldal aljat, visszavonjuk az esemenykovetest
     */
    trackScrolling = () => {
        let { loadedItems } = this.state;        
        const wrapper = document.querySelector('.contentWrapper');

        if (this.isBottom(wrapper)) {
            fetch(this.getFetchUrl(true))
            .then(res => res.json())
            .then(
                (result) => {
                    if (result.length) {
                        this.setState(prevState => ({
                            page: prevState.page + 1,
                            loadedItems: [...prevState.loadedItems].concat(result)
                        }), () => {
                            let recentLoadedItems = this.state.loadedItems;
                            this.props.actions.dispatch('loadedItems', recentLoadedItems);
                        });
                    }
                },
                (error) => {
                    console.log('hiba: ', error)
                }
            )
        }
    }


    /**
     * betoltjuk az active tab filterjeit / mappait     
     */
    loadTab() {
        let { openedTabs, tabPath } = this.state;
        let activeTab = this.findArrayElementById(openedTabs, 'id', tabPath);

        document.removeEventListener('scroll', this.trackScrolling);

        if (activeTab !== undefined) {
            if (activeTab.id !== null) {
                fetch(this.getFetchUrl())
                .then(res => res.json())
                .then(
                    (result) => {
                        this.setState({
                            page: 1,
                            loadedItems: result,
                            loading: false
                        }, () => {
                            document.addEventListener('scroll', this.trackScrolling);
                            this.props.actions.dispatch('loadedItems', this.state.loadedItems);
                        });
                    },
                    (error) => {
                        console.log('hiba: ', error)
                    }
                )
            }
        }
    }


    /**
     * lekerjuk egy custom filter adatait
     * ----------------------        
     * @param {integer} filterID:     filter numerikus azonositoja     
     */
    getFilter(filterID) {
        this.props.action("filter/" + filterID, "GET", null, function() {
            console.log('siker')
        });
    }


    /**
     * megvizsgalja, hogy egy adott poszt benne van-e egy adott mappaban
     * ----------------------        
     * @param {integer} folderID:     folder numerikus azonositoja
     * @param {integer} postID:       post numerikus azonositoja
     */
    inFolder(folderID, postID) {
        // let post = this.findArrayElementById(this.state.loadedItems, 'post_ID', postID);

        this.props.actions.subscribe('loadedItems', (data) => {
            console.log(data);
        });

        // if (post.length) {
        //     let result = this.findArrayElementById(post.folders, 'id', folderID);
            
        //     if (result.length) {
        //         return true;
        //     } else {
        //         return false;
        //     }
        // }
    }


    /**
     * torol egy posztot a folderbol
     * ----------------------        
      * @param {object} event: kattintas esemeny objektuma
     */
    removeFolder = (event) => {
        let self     = this; 
        let target   = event.target.closest('button');
        let postID   = target.getAttribute('data-item-id');
        let folderID = target.getAttribute('data-folder-id');
        let blogID   = target.getAttribute('data-blog-id');

        if (postID !== null && folderID !== null && blogID !== null) {
            this.props.action("folder/remove-item?post_id=" + postID + "&folder_id=" + folderID + "&blog_id=" + blogID, 'DELETE', null, function() {
                self.props.actions.dispatch('refreshFolders', true);
                self.loadTab();
            });
        }
    }


    /**
     * Olvasatlanna jelol egy posztot
     * ----------------------        
     * @param {integer} postID:     poszt numerikus azonositoja  
     * @param {boolean} read:       olvasott / nem olvasott
     */
    removePostReadStatus(postID, read) {
        if (read) {
            this.props.action("post-list/toggle-read?id=" + postID, "DELETE", null, function() {
                console.log('siker')
            });
        }
    }


    /**
     * Olvasatlanna jelol egy posztot
     */
    flagPostUnseen = (event) => {
        let postId = parseInt(event.currentTarget.getAttribute('data-item-id'));

        event.currentTarget.disabled = true;

        this.removePostReadStatus(postId, true);
        this.loadTab();
    }


    /**      
     * visszadja egy adott menupont eleresi utvonalat
     * ----------------------        
     * @param {string} path: szoveges tab index
     * @return {object}: ha a szoveges tab index alapjan filterre keresunk, megkeressuk az egyezo elemet az filtersObj-ben,
     *                   ha a szoveges tab index alapjan konyvtarra keresunk, visszaadunk egy statikus objektumot
     */
    getItemByPath(path) {
        if (path.indexOf('folder-') === -1) { //ha filter
            if(path.indexOf('custom') === -1) { //ha default filter
                const iconsObj = [{"id": 0, "path": "all", "iconClass": "fa-globe" },  
                                {"id": 1, "path": "hirdeteses", "iconClass": "fa-usd" },  
                                {"id": 2, "path": "holdudvar", "iconClass": "fa-users" },  
                                {"id": 3, "path": "kitiltottak", "iconClass": "fa-ban" },  
                                {"id": 4, "path": "copycat", "iconClass": "fa-files-o" },  
                                {"id": 5, "path": "nosalestagall", "iconClass": "fa-window-close" },  
                                {"id": 6, "path": "nosalestagfiltered", "iconClass": "fa-window-close" }];

                return iconsObj.find((element) => {
                    return element.path === path;
                })              
            } else { //ha custom filter
                return { "id": 7, "path": path, "iconClass": "fa-filter" };
            }
        } else { //ha konyvtar
            return { "id": 8, "path": path, "iconClass": "fa-folder" };
        }       
    }


    /**
     * elem keresese tombben, id alapjan
     * ----------------------        
     * @param {array} array: a tomb, amiben keresunk
     * @param {string} prop: az objektum propja
     * @param {string} id:   az id, amelyre keresunk (valojaban id === path)
     * @return {object}:     az object a tombben, amelynek megegyezik az id-ja
     */
    findArrayElementById(array, prop, id) {
        return array.find((element) => {
            return element[prop] === id;
        })
    }


    /**
     * poszt szerkeszto modal megnyitasa
     * frissiti a state-ben tarolt openedModal es openedModalPostId ertekeit
     * ----------------------        
     * @param {object} event: kattintas esemeny objektuma
     */
    openModalEditor = (event) => {
        let postId = parseInt(event.currentTarget.getAttribute('data-item-id'));

        this.setState({
            openedModal: true,
            openedModalPostId: postId
        });
    }


    /**
     * poszt szerkeszto modal bezarasa
     * minden modallal kapcsolatos state-et resetelunk
     */
    closeModalEditor = () => {
        this.setState({ 
            openedModalFolderEditor: false,
            openedModalFilterEditor: false,
            openedModal: false, 
            openedModalPostId: null,
            customFilterId: null,
            folderId: null,
            selectedFolderId: [],
            selectedSalestags: [],
        });
    }


    /**
     * visszaadja egy post indexet a loadedItems tombbol postID alapjan
     * ----------------------        
     * @param {integer} postID: 
     */
    getPostIndex = (postID) => {
        let ret;
        let loadedItems = this.state.loadedItems; 

        for (var i = 0; i < loadedItems.length; i++) {
            if (loadedItems[i].post_ID === postID) {
                ret = { "index": i, "post_ID": postID };
            }
        }       

        return ret;
    }


    /**
     * elore lapozas a poszt szerkeszto modalban
     * ----------------------        
     * @param {object} event: kattintas esemeny objektuma
     */
    nextPost = (event) => {
        let { loadedItems, openedModalPostId } = this.state;
        
        if (event) {
            event.stopPropagation();
        }

        if (openedModalPostId) {
            let currentPostIndex = this.getPostIndex(openedModalPostId).index;
            let newPostIndex = currentPostIndex+1;

            if (newPostIndex < loadedItems.length) {
                this.setState({
                    openedModal: true,
                    openedModalPostId: loadedItems[newPostIndex].post_ID
                }, () => {
                    // console.log(loadedItems.length, this.getPostIndex(this.state.openedModalPostId))
                    this.props.actions.dispatch('modalPostIdChanged', this.state.loadedItems[newPostIndex].post_ID);
                });
            }

            //mielott elerjuk az utolso betoltott elemet, betoltjuk a kovetkezo oldalt
            if (newPostIndex === loadedItems.length-1) {
                fetch(this.getFetchUrl(true))
                .then(res => res.json())
                .then(
                    (result) => {
                        if (result.length) {
                            this.setState(prevState => ({
                                page: prevState.page + 1,
                                loadedItems: [...prevState.loadedItems].concat(result)
                            }), () => {
                                let recentLoadedItems = this.state.loadedItems;
                                this.props.actions.dispatch('loadedItems', recentLoadedItems);
                            });
                        }
                    },
                    (error) => {
                        console.log('hiba: ', error)
                    }
                )
            }
        }
    }


    /**
     * vissza fele lapozas a poszt szerkeszto modalban
     * ----------------------        
     * @param {object} event: kattintas esemeny objektuma
     */
    prevPost = (event) => {
        let { loadedItems, openedModalPostId } = this.state;
        
        if (event) {
            event.stopPropagation();
        }
        
        if (openedModalPostId) {
            let currentPostIndex = this.getPostIndex(openedModalPostId).index;
            let newPostIndex = currentPostIndex-1;

            if (newPostIndex >= 0) {
                this.setState({
                    openedModal: true,
                    openedModalPostId: loadedItems[newPostIndex].post_ID
                }, () => {
                    // console.log(loadedItems.length, this.getPostIndex(this.state.openedModalPostId))
                    this.props.actions.dispatch('modalPostIdChanged', loadedItems[newPostIndex].post_ID);
                });
            }
        }
    }


    /**
     * konyvtar kivalasztasa
     * frissiti a state-ben tarolt selectedFolderId erteket
     * ----------------------        
     * @param {object} event: kattintas esemeny objektuma
     */
    selectFolder = (postID, blogID, event) => {
        let self = this;
        let currentFolderID = this.findArrayElementById(this.state.loadedItems, 'post_ID', postID).folder_id;
        let newFolderID = event.target.value;

        if (currentFolderID !== null) { //ha mar benne van egy mappaba
            self.props.changeFolder(currentFolderID, newFolderID, postID, blogID, function() {
                self.props.loadTab();
            });
        } else { //ha meg nincs benn mappaba
            self.props.addFolder(postID, newFolderID, blogID, function() {
                self.props.loadTab();
            });
        }

        this.setState({ selectedFolderId: newFolderID });
        this.props.actions.dispatch('refreshFolders', true);
    }


    /**
     * 4 fo blog tag inicializalasahoz
     * ha megtalalhato a megadott tag a blog tagjei kozott, active chip-et return-ol
     * ----------------------        
     * @param {string} tag:      blog tag neve
     * @param {array} blog_tags: a blog osszes tagje
     * @return {object}:         material ui chip komponens a megadott tagnevvel
     */
    initBlogTagChip(tag, blog_tags) {
        let html;        
        
        if (blog_tags.indexOf(tag) !== -1) {
            html = <Chip label={tag} clickable onClick={this.toggleBlogTagChip} className={[classes.chip, 'active'].join(' ')} />;
        } else {
            html = <Chip label={tag} clickable onClick={this.toggleBlogTagChip} className={classes.chip} />;
        }          

        return html;
    }

    
    /**
     * 4 fo blog tag ki / bekapcsolasa
     * ----------------------        
     * @param {string} event: kattintas esemeny objektuma    
     */
    toggleBlogTagChip(event) {
        if (event.target.classList[0] === "MuiChip-label") {
            event.target.parentElement.classList.toggle('active');
        } else {
            event.target.classList.toggle('active');
        }        
    }
    

    /**
     * lista elem megjelenitese formazott datummal
     * ----------------------        
     * @param {string} tabPath: szoveges eleresi utvonal
     * @param {object} item:    betoltott poszt objektuma
     */
    displayList = (tabPath, item) => {
        let isFilter    = tabPath.indexOf('folder-') === -1;
        let listItemStyle = (!item.post_read ? 'title bold' : 'title');
        let pubDateObj  = new Date(item.post_datestart); 
        let pubDate     = pubDateObj.toLocaleDateString('hu', {
            weekday: "long",
            year: "numeric",
            month: "long",
            day: "numeric"
        });

        return (
            isFilter            
            ? <Link style={{display: 'inline-block', verticalAlign: 'middle'}} underline="none" component={RouterLink} to={`${tabPath}/${item.post_ID}`}><ListItemText component="span" className={listItemStyle} primary={item.post_title} secondary={`${item.blog_name} - ${pubDate}`} /></Link>
            : <Link style={{display: 'inline-block', verticalAlign: 'middle'}} underline="none" component={RouterLink} to={`/all/${item.post_ID}`}><ListItemText component="span" className={listItemStyle} primary={item.post_title} secondary={`${item.blog_name} - ${pubDate}`} /></Link>
        );                                                
    }


    /**
     * formazott hibauzenet
     * ----------------------        
     * @param {string} txt:     a hibauzenet szovege
     * @return {object}:        a hibauzenet HTML-je
     */
    displayError(txt) {
        return <div className="errorMessage"><p><i className={["fa","fa-frown-o"].join("")} aria-hidden="true"></i></p><p>{txt}</p></div>;
    }


    /**
     * szuro frissito esemeny, frissiti a state-ben tarolt filters-t es atadjuk a modal filter editornak
     * ----------------------        
     * @param {array} newFilter:     befrissult szurok tombje
     */
    onFilterChange(newFilter) {
        this.setState({ 
            filters: newFilter 
        });
    }


    /**
     * mappa frissito esemeny, frissiti a state-ben tarolt folders-t es atadjuk a modal folder editornak
     * ----------------------        
     * @param {array} newFolder:     befrissult mappak tombje
     */
    onFolderChange(newFolder) {
        this.setState({ 
            folders: newFolder
        });
    }

    
    /**      
     * itt rendereljuk ki a lista komponensunket a kattintasra triggerelodo modal ablakkal
     */
    render() {
        let modal;
        let modalPostEditorBody;
        let modalPostEditorHead;
        let { path } = this.props.routing;        
        let { loadedItems, filters, folders, openedModal, openedModalPostId, openedModalTitle, openedModalFilterEditor, openedModalFolderEditor, selectedFolderId, allSalestags, selectedSalestags, customFilterId, folderId, ...other } = this.state;
        let item = this.findArrayElementById(loadedItems, 'post_ID', openedModalPostId);        

        //ha van modal
        if ( openedModal ) {

            //filter hozzaadasa / szerkesztese
            if ( openedModalFilterEditor ) {
                modalPostEditorHead = <h2 id="modal-title">{openedModalTitle}</h2>;
                modalPostEditorBody = (
                    <ModalFilterEditor 
                    classes={classes}
                    filters={filters}
                    action={this.props.action}
                    actions={this.props.actions}
                    customFilterId={customFilterId}
                    closeModalEditor={this.closeModalEditor}
                    onFilterChange={this.onFilterChange}
                    />
                );
            }


            //folder hozzaadasa / szerkesztese
            if ( openedModalFolderEditor ) {
                modalPostEditorHead = <h2 id="modal-title">{openedModalTitle}</h2>;
                modalPostEditorBody = (
                    <ModalFolderEditor 
                    classes={classes}
                    folders={folders}
                    action={this.props.action}
                    actions={this.props.actions}
                    folderId={folderId}
                    closeModalEditor={this.closeModalEditor}
                    onFolderChange={this.onFolderChange}
                    />
                );
            }


            //modal posztszerk.
            if ( openedModalPostId && item ) {
                modalPostEditorHead = (<div>
                    <IconButton
                    title="Előre"
                    id="right"                    
                    className="pull-right"
                    onClick={this.nextPost}
                    >
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <ArrowForwardIosIcon />
                        </svg>
                    </IconButton>

                    <IconButton
                    title="Vissza"
                    id="left"                    
                    className="pull-left"
                    onClick={this.prevPost}
                    >
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <ArrowForwardIosIcon />
                        </svg>
                    </IconButton>

                    <h2 id="modal-title" title={!item.post_title.trim().length ? 'NINCS CÍM' : item.post_title}>{!item.post_title.trim().length ? 'NINCS CÍM' : item.post_title}</h2>

                    {
                        item.post_read === true 
                        ? <IconButton
                            title="Poszt megjelölése olvasatlanként"
                            data-item-id={item.post_ID}
                            onClick={this.flagPostUnseen}
                            >
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <path d="M19.604 2.562l-3.346 3.137c-1.27-.428-2.686-.699-4.243-.699-7.569 0-12.015 6.551-12.015 6.551s1.928 2.951 5.146 5.138l-2.911 2.909 1.414 1.414 17.37-17.035-1.415-1.415zm-6.016 5.779c-3.288-1.453-6.681 1.908-5.265 5.206l-1.726 1.707c-1.814-1.16-3.225-2.65-4.06-3.66 1.493-1.648 4.817-4.594 9.478-4.594.927 0 1.796.119 2.61.315l-1.037 1.026zm-2.883 7.431l5.09-4.993c1.017 3.111-2.003 6.067-5.09 4.993zm13.295-4.221s-4.252 7.449-11.985 7.449c-1.379 0-2.662-.291-3.851-.737l1.614-1.583c.715.193 1.458.32 2.237.32 4.791 0 8.104-3.527 9.504-5.364-.729-.822-1.956-1.99-3.587-2.952l1.489-1.46c2.982 1.9 4.579 4.327 4.579 4.327z"/>
                                </svg>
                            </IconButton>
                        : ''
                    }
                </div>);

                modalPostEditorBody = (
                    <div>
                        <ModalPostEditor 
                        classes={classes}
                        item={item}
                        folders={folders}
                        actions={this.props.actions}
                        loadedItems={loadedItems}
                        openedModalPostId={openedModalPostId}
                        action={this.props.action}
                        allSalestags={allSalestags}
                        selectedSalestags={selectedSalestags}
                        selectedFolderId={selectedFolderId}
                        initBlogTagChip={this.initBlogTagChip}
                        selectFolder={this.selectFolder}
                        findArrayElementById={this.findArrayElementById}
                        prevPost={this.prevPost}
                        nextPost={this.nextPost}
                        {...other}
                        />
                    </div>
                );
            }

            modal = (<ModalComponent
                    openedModal={openedModal}
                    modalBody={modalPostEditorBody}
                    modalHead={modalPostEditorHead}
                    closeModalEditor={this.closeModalEditor}
                    classes={classes}
                    />);
        }

    	return (
            <React.Fragment>
                <Paper>
            		<List className={classes.root}>
                        {this.state.loading === false ?
                			<React.Fragment>
                                {
                                !loadedItems.length 
                                ? this.displayError('Ebben a szűrőben sajnos nincs megjeleníthető poszt')
                                : loadedItems.map((item, key) => 
                                        <React.Fragment key={key}>                                            
                                            <DraggableListItem
                                            item={item}
                                            loadedItems={loadedItems}
                                            action={this.props.action}
                                            actions={this.props.actions}
                                            tabPath={this.props.tabPath}
                                            displayList={this.displayList}
                                            openModalEditor={this.openModalEditor}
                                            addFolder={this.props.addFolder}
                                            removeFolder={this.removeFolder}
                                            changeFolder={this.props.changeFolder}
                                            findArrayElementById={this.findArrayElementById}                                            
                                            />   
                                            
                                        </React.Fragment>
                                    )
                                }                               
                    	    </React.Fragment>
                            : <Loading />
                        }
            		</List>
                </Paper>

                {modal}
            </React.Fragment>
		);
    }
}