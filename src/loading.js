import React from 'react';

export default class Loading extends React.Component {
	render() {
		return <div className="loading">
			<img alt="Töltés" src="data:image/svg+xml;charset=US-ASCII,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%0A%3Csvg%20width%3D%22100%22%20height%3D%22100%22%20version%3D%221.1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%20xml%3Aspace%3D%22preserve%22%3E%0A%20%20%20%20%3ClinearGradient%20id%3D%22linearColors%22%20x1%3D%220%22%20y1%3D%220%22%20x2%3D%221%22%20y2%3D%221%22%3E%0A%20%20%20%20%20%20%20%3Cstop%20offset%3D%220%25%22%20stop-color%3D%22%23ffffff%22%3E%3C%2Fstop%3E%0A%20%20%20%20%20%20%20%3Cstop%20offset%3D%22100%25%22%20stop-color%3D%22%23f47629%22%3E%3C%2Fstop%3E%0A%20%20%20%20%3C%2FlinearGradient%3E%0A%0A%09%3Ccircle%20%0A%09r%3D%2235%22%0A%09cx%3D%2250%22%0A%20%20%20%20cy%3D%2250%22%0A%09class%3D%22external-circle%22%20%0A%09stroke-width%3D%228%22%20%0A%09fill%3D%22none%22%20%0A%09stroke%3D%22url(%23linearColors)%22%0A%09transform%3D%22rotate(275.845%2050%2050)%22%0A%09%3E%0A%09%09%3CanimateTransform%0A%09%09attributeName%3D%22transform%22%0A%09%09type%3D%22rotate%22%0A%09%09calcMode%3D%22linear%22%0A%09%09values%3D%220%2050%2050%3B360%2050%2050%22%0A%09%09keyTimes%3D%220%3B1%22%0A%09%09dur%3D%221s%22%0A%09%09begin%3D%220s%22%0A%09%09repeatCount%3D%22indefinite%22%0A%09%09%2F%3E%0A%09%3C%2Fcircle%3E%0A%3C%2Fsvg%3E" />
		</div>;
	}
}