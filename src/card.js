import React from 'react';
import Interweave from 'interweave';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActionArea from '@material-ui/core/CardActionArea';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import Divider from '@material-ui/core/Divider';
import { Link as RouterLink } from 'react-router-dom'
import Link from '@material-ui/core/Link';
import Fade from '@material-ui/core/Fade';
import Loading from './Loading';

class CardComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [], //kartyak 
            loading: true
        };

        this.loadTab = this.loadTab.bind(this);
        // this.fetchData = this.fetchData.bind(this);
    }

    /*
     * betoltjuk json-bol a kartyakat
     * es hozzaadjuk oket az items tombhoz
     */
    componentDidMount() {
        //feliratkozunk az aktiv tab indexere es rogzitjuk a state-ben
        this.props.actions.subscribe('tabIndex', (tabIndexData) => {
            this.setState({ tabIndex: tabIndexData }, () => { 
                console.log(this.state.tabIndex);
            });
        });

        //feliratkozunk a megnyitott tabokra es rogzitjuk oket a state-ben
        this.props.actions.subscribe('openedTabs', (openedTabsData) => {
            this.setState({ openedTabs: openedTabsData }, () => { 
                console.log(this.state.openedTabs) 
            });            
        });
                
        this.loadTab();
    }

    componentWillUnmount() {
        this.props.actions.retain('viewType');
        this.props.actions.retain('tabIndex');
    }

    // fetchData(type) {
    //     let jsonURL = "";

    //     switch (type) {
    //         default:
    //         case 0:
    //         jsonURL = "./json/hirdeteses.json";
    //         break;

    //         case 1:
    //         jsonURL = "./json/all.json";
    //         break;
    //     }

    //     fetch(jsonURL)
    //     .then(res => res.json())
    //     .then(
    //         (result) => {
    //             this.setState({
    //                 items: result || result.data,
    //                 loading: false
    //             }, () => {
    //                 this.props.actions.dispatch('loadedItems', this.state.items);
    //             });
    //         }
    //     )
    // }

    findArrayElementById(array, prop, id) {
        return array.find((element) => {
            return element[prop] === id;
        })
    }

    loadTab() {
        let activeTab = this.findArrayElementById(this.state.openedTabs, 'id', this.state.tabIndex);
        console.log('activeTab', activeTab);
        
        fetch("./json/" + activeTab.path + ".json")
        // fetch("/reader/post-list/filter?id=" + activeTab.path)
        .then(res => res.json())
        .then(
            (result) => {
                this.setState({
                    items: result,
                    loading: false
                }, () => {
                    this.props.actions.dispatch('loadedItems', this.state.items);
                    console.log('a loadedtab lefutott:', this.state)
                });
            },
            (error) => {
                console.log('hiba: ', error)
            }
        )
    }

    /* get youtube video id from string */ 
    getYoutubeVideo(str) {
        var ID = '';
        
        str = str.replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);

        if(str[2] !== undefined) {
            ID = str[2].split(/[^0-9a-z_\-]/i);
            ID = ID[0];
        }
        else {
            ID = false;
        }
        
        return ID;
    }

    /*  
     *  get post first image 
     *  if there is no image, it tries to search for youtube video
     *  if no video neither it returns false
     */ 
    getPostFirstImg(str) {
        var img = /<img.*?src=\"(.*?)\"/;
        var src = img.exec(str);

        if (src) {
            return src[1];
        } else {
            var yt = this.getYoutubeVideo(str);

            if (yt !== false) {
                return "https://img.youtube.com/vi/" + yt + "/0.jpg";
            } else {
                return false;
            }
        }
    }

    render() {
        let { path } = this.props.routing;       

        const classes = makeStyles(theme => ({
            chip: {
                margin: theme.spacing(1),
            },
            card: {
                maxWidth: 345,
            },
            media: {
                height: 0,
                paddingTop: '56.25%', // 16:9
            },
            expand: {
                transform: 'rotate(0deg)',
                marginLeft: 'auto',
                transition: theme.transitions.create('transform', {
                    duration: theme.transitions.duration.shortest,
                }),
            },
            expandOpen: {
                transform: 'rotate(180deg)',
            },
            avatar: {
                backgroundColor: red[500],
            },
            paper: {
                backgroundColor: theme.palette.background.paper,
                border: '2px solid #000',
                boxShadow: theme.shadows[5],
                padding: theme.spacing(2, 4, 3),
            },
            formControl: {
                margin: theme.spacing(1),
                minWidth: 120,
            },            
            heading: {
                fontSize: theme.typography.pxToRem(20),
                fontWeight: theme.typography.fontWeightRegular,
            },
        }));

        const coverStyle = {
            height: '200px'
        };

        return ( 
            <React.Fragment>
            {this.state.loading === false ?
                <React.Fragment>
                    {
                        this.state.items.map((item, key) => 
                            <React.Fragment key={key}>                                  
                                <Fade in={true} timeout={key*500}>
                                    <Card className={[classes.card, 'card'].join(' ')}>
                                        <CardActionArea component="div">
                                            <Link
                                            component={RouterLink}
                                            to={`${path}/${item.post_ID}`}
                                            >
                                                <CardHeader
                                                    avatar={
                                                        <Avatar aria-label="recipe" className={classes.avatar}>
                                                            {item.author}
                                                        </Avatar>
                                                    }

                                                    title={item.post_title}
                                                    subheader={item.post_datestart}
                                                />

                                                {this.getPostFirstImg(item.post_content) ? (
                                                    <CardMedia
                                                        className={classes.media}
                                                        image={this.getPostFirstImg(item.post_content)}
                                                        title={item.post_title}
                                                        style={coverStyle}
                                                    />                                                    
                                                ) : null}

                                                <CardContent>
                                                    <Typography variant="body2" color="textSecondary" component="p" noWrap>                                                    
                                                        <Interweave noHtml content={item.post_content} />
                                                    </Typography>
                                                </CardContent>
                                            </Link>
                                        </CardActionArea>
                                    </Card>
                                </Fade>

                                {key < this.state.items.length-1 ? <Divider className='divi' /> : ''}
                            </React.Fragment>
                        )
                    }
                </React.Fragment>
                : <Loading />
            }
            </React.Fragment>
        );
    }
}

export default CardComponent; 