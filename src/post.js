//importalt komponensek
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import Divider from '@material-ui/core/Divider';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Typography from '@material-ui/core/Typography';

//material ui styles
const useStyles = makeStyles(theme => ({
		root: {
			flexGrow: 1,
			backgroundColor: theme.palette.background.paper,
			display: 'flex',
			height: 224,
		},
		formControl: {
			margin: theme.spacing(1),
			minWidth: '100%',
			maxWidth: 300,
		},
		chips: {
			display: 'flex',
			flexWrap: 'wrap',
		},
		chip: {
			margin: 2,
		}
	}));


//elvalaszto csik + cimke ikon
const TagIcon = () => {
	return (
		<span>
			<Divider className="hr" />
			<i className={['fa','fa-tags'].join(' ')}></i>			
		</span>
	);
};


export default class PostComponent extends React.Component {

	/**
	 * a constructor state-jeben tarolunk minden erteket, 
	 * amelyre a komponensunknek szuksege lesz
	 */
	constructor(props) {
		super(props);

		this.state = {
			loadedItems: this.props.loadedItems, //betoltott itemek 
			allSalestags: this.props.allSalestags, //salestagek
			selectedSalestags: [], //kivalasztott salestagek
		};

		this.addPostReadStatus = this.addPostReadStatus.bind(this);
		this.removePostReadStatus = this.removePostReadStatus.bind(this);
		this.togglePostTag = this.togglePostTag.bind(this);
		this.toggleBlogTagChip = this.toggleBlogTagChip.bind(this);

		this.salesTagHandleChange = this.salesTagHandleChange.bind(this);
		this.removeSalestag = this.removeSalestag.bind(this);
		this.addSalestag = this.addSalestag.bind(this);		
	}


	/**
	 * triggereljuk a poszt komponens unmountolasat
	 */
	dismiss() {
        this.props.handleUnmount('post');
    } 

    	
	/**
	 * itt minden lefut, amint a komponens mountolodott
	 */
	componentDidMount() {
		let { loadedItems } = this.state;

		//ha van betoltott poszt
		if (loadedItems.length) {
			let itemId 		= parseInt(this.props.routing.match.params.id);
			let loadedItem  = this.findArrayElementById(loadedItems, 'post_ID', itemId);
			
			//olvasotta tesszuk
			this.addPostReadStatus(itemId, loadedItem.post_read);

			//betoltjuk a kivalasztott salestaget, ha van
			if (loadedItem.salestags !== null) {
				this.setState({
					selectedSalestags: Object.values(loadedItem.salestags)
				});
			}
		}
	}


	/**
 	 * salestag hozzaadasa poszthoz
 	 * ----------------------    	 
	 * @param {integer} postID: 	poszt numerikus azonositoja
	 * @param {integer} salestagID: salestag numerikus azonositoja
	 */
	addSalestag(postID, salestagID) {
		this.props.action("post-list/add-salestag?id=" + postID + "&salestag_id=" + salestagID, 'PUT', null, function() {
			console.log('siker')
		});
	}


	/**
 	 * salestag torlese posztrol
 	 * ----------------------    	 
	 * @param {integer} postID: 	poszt numerikus azonositoja
	 * @param {integer} salestagID: salestag numerikus azonositoja
	 */
	removeSalestag(postID, salestagID) {
		this.props.action("post-list/remove-salestag?id=" + postID + "&salestag_id=" + salestagID, 'DELETE', null, function() {
			console.log('siker')
		});
	}


	/**
 	 * felrak / leszed egy taget egy posztrol
 	 * ----------------------    	 
	 * @param {integer} postID: 	poszt numerikus azonositoja
	 * @param {string} tag: 		tag neve
	 * @param {string} method: 		fetch method (PUT / DELETE)
	 */
	togglePostTag(postID, tag, method) {
		this.props.action("post-list/toggle-post-tag?id=" + postID + "&tag=" + tag, method, null, function() {
			console.log('siker')
		});
	}


	/**
 	 * Olvasotta jelol egy posztot
 	 * ----------------------    	 
	 * @param {integer} postID: 	poszt numerikus azonositoja	 
	 * @param {boolean} read: 		olvasott / nem olvasott
	 */
	addPostReadStatus(postID, read) {
		if (!read) {
			this.props.action("post-list/toggle-read?id=" + postID, 'PUT', null, function() {
				console.log('siker')
			});
		}
	}


	/**
 	 * Olvasatlanna jelol egy posztot
 	 * ----------------------    	 
	 * @param {integer} postID: 	poszt numerikus azonositoja	 
	 * @param {boolean} read: 		olvasott / nem olvasott
	 */
	removePostReadStatus(postID, read) {
		if (read) {
			this.props.action("post-list/toggle-read?id=" + postID, 'DELETE', null, function() {
				console.log('siker')
			});
		}
	}


    /**
 	 * elem keresese tombben, id alapjan
 	 * ----------------------    	 
	 * @param {array} array: a tomb, amiben keresunk
	 * @param {string} prop: az objektum propja
	 * @param {string} id: 	 az id, amelyre keresunk (valojaban id === path)
	 * @return {object}: 	 az object a tombben, amelynek megegyezik az id-ja
	 */
    findArrayElementById(array, prop, id) {
    	return array.find((element) => {
    		return element[prop] === id;
    	})
    }


    /**
 	 * kiszedi az author_url-bol a user id-t
 	 * ----------------------    	 
	 * @param {string} url: a user id-t tartalmazo url
	 * @return {integer}: 	numerikus user id
	 */ 
    getUserId(url) {
    	return url.match(/\d+/)[0];
    }


    /**
 	 * salestagek ki / bekapcsolasa (multiple)
 	 * befrissiti a state-ben tarolt selectedSalestags objektumot
 	 * ----------------------    	 
	 * @param {string} event: kattintas esemeny objektuma
	 * @param {string} index: salestag indexe
	 */ 	
    /*salesTagHandleChange = (event, index) => {
    	//ha mar benne van a kivalasztott salestagekben
    	if (this.state.selectedSalestags.indexOf(index.key) !== -1) {
    		var salestags = [...this.state.selected_salestags];
			var tagIndex  = salestags.indexOf(index.key);

    		salestags.splice(tagIndex, 1);
    		this.setState({
    			selectedSalestags: salestags
    		}, () => {
	    		console.log(this.state.selectedSalestags)
	    	});	    	
    	} else { //ha meg nincs benne
	    	this.setState(prevState => ({
	    		selectedSalestags: [...prevState.selectedSalestags, index.key]
	    	}), () => {
	    		console.log(this.state.selectedSalestags)
	    	});
    	}		
    }*/


    /**
     * salestagek ki / bekapcsolasa (single)
     * befrissiti a state-ben tarolt selectedSalestags objektumot
     * ----------------------        
     * @param {string} event: kattintas esemeny objektuma
     * @param {string} index: salestag indexe
     */ 
    salesTagHandleChange = (blogId, event) => {
    	let { selectedSalestags, allSalestags } = this.state;
    	let salestagId = parseInt(this.findArrayElementById(allSalestags, 'salestag_tag', Object.values(selectedSalestags)[0]).salestag_ID);

    	console.log(salestagId, blogId);
    	console.log(selectedSalestags, event.target.value)

        this.setState({
            selectedSalestags: [event.target.value]
        }, () => {
            console.log(selectedSalestags)
        })
    }


    /**
     * 4 fo blog tag inicializalasahoz
     * ha megtalalhato a megadott tag a blog tagjei kozott, active chip-et return-ol
     * ----------------------        
     * @param {string} tag:      blog tag neve
     * @param {array} blog_tags: a blog osszes tagje
     * @return {object}:         material ui chip komponens a megadott tagnevvel
     */
    initBlogTagChip(tag, blog_tags) {
        let html;        
        
        if (blog_tags.indexOf(tag) !== -1) {
            html = <Chip label={tag} clickable onClick={this.toggleBlogTagChip} className={[useStyles.chip, 'active'].join(' ')} />;
        } else {
            html = <Chip label={tag} clickable onClick={this.toggleBlogTagChip} className={useStyles.chip} />;
        }          

        return html;
    }

    
    /**
     * 4 fo blog tag ki / bekapcsolasa
     * ----------------------        
     * @param {string} event: kattintas esemeny objektuma    
     */
    toggleBlogTagChip(event) {
    	if (localStorage.getItem('post_ID') !== null) {
	    	let el, method, tag,
	    		postID = parseInt(localStorage.getItem('post_ID'));

	        if (event.target.classList[0] === "MuiChip-label") {	            
				el  = event.target.parentElement;
	        } else {
	            el  = event.target;	            
	        }

            tag = el.textContent.trim();
            el.classList.toggle('active');
	        
	        //method beallitasa
	        if (el.classList.contains('active')) {
				method = 'PUT';
	        } else {
				method = 'DELETE';
	        }

	        //action hivas
	        this.togglePostTag(postID, tag, method);
    	}
    }


    /** 	 
 	 * itt rendereljuk ki a poszt komponensunk templatejet
	 */
	render() {
		let template;

	    //ha van eleme a loadedItems-nek, betoltjuk a templatet
		if (this.state.loadedItems.length) {
			let itemId 		= parseInt(this.props.routing.match.params.id);		
			let loadedItems = this.state.loadedItems;
			let loadedItem  = this.findArrayElementById(loadedItems, 'post_ID', itemId);

			if (loadedItem) {
				let pubDateObj 	= new Date(loadedItem.post_datestart); 
				let pubDate 	= pubDateObj.toLocaleDateString('hu', {
					weekday: "long",
					year: "numeric",
					month: "long",
					day: "numeric"
				});

				//felvesszuk localstorage-ba az aktiv post id-t
				localStorage.setItem('post_ID', loadedItem.post_ID);

				//template a poszt oldalhoz
				template = <Paper className={this.props.useStyles.root}>
						        <div className={this.props.useStyles.paper}>					            
									<div id="page-wrapper">
										<div id="page-top-wrapper">
								            <h1 id="page-title">
								            	<a target="_blank" rel="noopener noreferrer" href={loadedItem.url}>
								            		{loadedItem.post_title}
								            	</a>
							            	</h1>
								            
								            <div id="page-details">
								            	<a rel="noopener noreferrer" target="_blank" href={loadedItem.blog_url}>{loadedItem.blog_name}</a> - {pubDate} by&nbsp;							            	
								            	<a rel="noopener noreferrer" target="_blank" href={`https://blog.hu/user/${this.getUserId(loadedItem.author_url)}/tab/data`}>{loadedItem.blog_name}</a>
								            	<a rel="noopener noreferrer" target="_blank" href={`https://blog.hu/user/${this.getUserId(loadedItem.author_url)}/tab/msg`}>&nbsp;
								            		<i className={['fa','fa-envelope'].join(' ')}></i>
						            			</a>
						            			<a rel="noopener noreferrer" target="_blank" href={`${loadedItem.blog_url}/admin/post/edit/${loadedItem.post_ID}`}>&nbsp;
								            		<i className={['fa','fa-pencil'].join(' ')}></i>
						            			</a>
								            </div>
										</div>
											
										<div id="page-content" dangerouslySetInnerHTML={{__html: loadedItem.post_content}}></div>
									</div>

						            <div id="page-body">					            	
						                <div id="page-expansion-panel">

						               		{/* TAGEK */}
						                    <ExpansionPanel>
						                        <ExpansionPanelSummary
						                        expandIcon={<ExpandMoreIcon />}
						                        aria-controls="panel1a-content"
						                        id="panel1a-header"
						                        >
						                            <Typography className={[this.props.useStyles.heading, 'expansion-panel-heading'].join(' ')}>Tagek</Typography>
						                        </ExpansionPanelSummary>
						                        <ExpansionPanelDetails>					                        	
													<FormControl className={[this.props.useStyles.formControl, 'mocsok'].join(' ')}>
								                        <InputLabel htmlFor="select-multiple-chip">Salestagek</InputLabel>
								                        <Select
		                                                value={this.state.selectedSalestags}
		                                                onChange={this.salesTagHandleChange.bind(null, loadedItem.post_blog_ID)}
		                                                inputProps={{
		                                                    name: 'salestags',
		                                                    id: 'select-salestag',
		                                                }}
		                                                >
		                                                    {
		                                                        this.state.allSalestags.map((item, key) => 
		                                                            (item.salestag_tag === "Salestagek" 
		                                                            ? <MenuItem value="" disabled>Salestagek</MenuItem>
		                                                            : <MenuItem key={key} value={item.salestag_tag}>{item.salestag_tag}</MenuItem>)
		                                                        )
		                                                    } 
		                                                </Select>
							                        </FormControl>

						                        	<div className="mocsok">
							                        	<i className={['fa','fa-tags'].join(' ')}></i>

						                                {this.initBlogTagChip('a:noszerkfeed', loadedItem.blog_tags)}
		                                                {this.initBlogTagChip('a:copycat', loadedItem.blog_tags)}
		                                                {this.initBlogTagChip('a:szponzorblog', loadedItem.blog_tags)}
		                                                {this.initBlogTagChip('a:holdudvar', loadedItem.blog_tags)}
					                                </div>
						                        </ExpansionPanelDetails>
						                    </ExpansionPanel>
											{/* TAGEK */}


											{/* BLOG */}
						                    <ExpansionPanel>
						                        <ExpansionPanelSummary
						                        expandIcon={<ExpandMoreIcon />}
						                        aria-controls="panel1a-content"
						                        id="panel1a-header"
						                        >
						                            <Typography className={[useStyles.heading, 'expansion-panel-heading'].join(' ')}>Blog</Typography>
						                        </ExpansionPanelSummary>
						                        <ExpansionPanelDetails>
						                            <div className="expansion-panel-cell">
						                                <b><i className={['fa','fa-info-circle'].join(' ')}></i> Státusz:</b> {loadedItem.blog_adstatus}<br />
						                                <b><i className={['fa','fa-eye'].join(' ')}></i> PI:</b> {loadedItem.blog_pi}<br />
						                                <b><i className={['fa','fa-users'].join(' ')}></i> UV:</b> {loadedItem.blog_uv}

						                                {loadedItem.blog_tags.length > 0 ? <TagIcon /> : ''}

						                                {loadedItem.blog_tags.length > 0
															? loadedItem.blog_tags.map((tag, index) => (
																<Chip label={tag} key={index} className={useStyles.chip} /> 
															))
															: null
														}
						                            </div>
						                        </ExpansionPanelDetails>
						                    </ExpansionPanel>
						                    {/* BLOG */}


						                	{/* POST */}
						                    <ExpansionPanel>
						                        <ExpansionPanelSummary
						                        expandIcon={<ExpandMoreIcon />}
						                        aria-controls="panel2a-content"
						                        id="panel2a-header"
						                        >
						                            <Typography className={[useStyles.heading, 'expansion-panel-heading'].join(' ')}>Post</Typography>
						                        </ExpansionPanelSummary>
						                        <ExpansionPanelDetails>
						                            <div className="expansion-panel-cell">
						                                <b><i className={['fa','fa-comments'].join(' ')}></i> <a rel="noopener noreferrer" target="_blank" href={`${loadedItem.url}#comments`}>Kommentek</a>:</b> {loadedItem.comment_count}
						                            
						                                {loadedItem.post_tags.length > 0 ? <TagIcon /> : ''}

														{loadedItem.post_tags.length > 0
															? loadedItem.post_tags.map((tag, index) => (
																<Chip label={tag} key={index} className={useStyles.chip} /> 
															))
															: null
														}
						                            </div>
						                        </ExpansionPanelDetails>
						                    </ExpansionPanel>
						                	{/* POST */}
						                </div>
						            </div>
						        </div>
						    </Paper>;
			} else {
				window.location.href = "/";  //ha nincs loadedItem, visszairanyitjuk a usert a fooldalra
			}
		}

		//returnoljuk a templatet
	    return (	    	
	    	<div className="contentWrapper">
	    		{template}
	    	</div>
		);
	}
}