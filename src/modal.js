import React from 'react'
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import Paper from '@material-ui/core/Paper';
import Loading from './loading';

/**
 * modal komponens
 * visszaad egy modalt a megadott parameterek alapjan
 */
const ModalComponent = (props) => {
    const { openedModal, modalBody, modalHead, classes } = props;
    
    return(        
        <Modal
        id="modal-backdrop"
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={openedModal}
        onClose={props.closeModalEditor}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
            timeout: 500,
        }}
        >
            <Fade in={openedModal}>
                <div id="modal" className={classes.paper}>
                    <Paper id="modal-inner">
                        {modalHead !== undefined ? modalHead : <h2 id="modal-title">Kis türelmet...</h2> }

                        <div id="modal-body">
                            {modalBody !== undefined ? modalBody : <Loading />}
                        </div>
                    </Paper>
                </div>
            </Fade>
        </Modal>
    );
}

export default ModalComponent;