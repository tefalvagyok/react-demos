import React from 'react';
import clsx from 'clsx';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';

//ikonok tarsitasa a ket allapothoz (success / error)
const variantIcon = {
	success: CheckCircleIcon,
	error: ErrorIcon
};


/**
 * snackbar komponens
 * hibauzenetek megjelenitesehez
 */
const SnackbarComponent = (props) => {	
	const Icon = variantIcon[props.variant];
	
	//snackbar bezaro fuggveny
	const handleClose = (event, reason) => {
		props.actions.dispatch('closeDialog', true);
	};

	return (
		<Snackbar
		anchorOrigin={{
			vertical: 'bottom',
			horizontal: 'right'
		}}
		open={true}
		autoHideDuration={1000}
		onEnter={props.closeSnackbar()}
		>
			<SnackbarContent
			  className={clsx(props.classes[props.variant], props.variant)}
	          variant={props.variant}
	          message={
	          	<span id="client-snackbar" className={props.classes.message}>
	          		<Icon className={clsx(props.classes.icon, props.classes.iconVariant)} />
	          		{props.message}
	          	</span>
	          }
	          action={[
	          	<IconButton key="close" aria-label="close" color="inherit" onClick={handleClose}>
	          		<CloseIcon className={props.classes.icon} />
	          	</IconButton>,
	          	]}
	        />
		</Snackbar>
	);
}

export default SnackbarComponent;